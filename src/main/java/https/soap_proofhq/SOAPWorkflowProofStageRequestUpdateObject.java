
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowProofStageRequestUpdateObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowProofStageRequestUpdateObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadline_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadline_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="start_trigger" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="start_dependent_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stage_locking" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_one_decision_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stage_private" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deadline_calculate_on" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowProofStageRequestUpdateObject", propOrder = {
    "stageId",
    "name",
    "deadlineDate",
    "deadlineTime",
    "startTrigger",
    "startDependentStageId",
    "startDependentDate",
    "startDependentTime",
    "stageLocking",
    "stageOneDecisionOnly",
    "stagePrivate",
    "deadlineCalculateOn"
})
public class SOAPWorkflowProofStageRequestUpdateObject {

    @XmlElement(name = "stage_id")
    protected int stageId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(name = "deadline_date", required = true)
    protected String deadlineDate;
    @XmlElement(name = "deadline_time", required = true)
    protected String deadlineTime;
    @XmlElement(name = "start_trigger")
    protected int startTrigger;
    @XmlElement(name = "start_dependent_stage_id")
    protected int startDependentStageId;
    @XmlElement(name = "start_dependent_date", required = true)
    protected String startDependentDate;
    @XmlElement(name = "start_dependent_time", required = true)
    protected String startDependentTime;
    @XmlElement(name = "stage_locking")
    protected int stageLocking;
    @XmlElement(name = "stage_one_decision_only")
    protected boolean stageOneDecisionOnly;
    @XmlElement(name = "stage_private")
    protected boolean stagePrivate;
    @XmlElement(name = "deadline_calculate_on")
    protected int deadlineCalculateOn;

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the deadlineDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadlineDate() {
        return deadlineDate;
    }

    /**
     * Sets the value of the deadlineDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadlineDate(String value) {
        this.deadlineDate = value;
    }

    /**
     * Gets the value of the deadlineTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadlineTime() {
        return deadlineTime;
    }

    /**
     * Sets the value of the deadlineTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadlineTime(String value) {
        this.deadlineTime = value;
    }

    /**
     * Gets the value of the startTrigger property.
     * 
     */
    public int getStartTrigger() {
        return startTrigger;
    }

    /**
     * Sets the value of the startTrigger property.
     * 
     */
    public void setStartTrigger(int value) {
        this.startTrigger = value;
    }

    /**
     * Gets the value of the startDependentStageId property.
     * 
     */
    public int getStartDependentStageId() {
        return startDependentStageId;
    }

    /**
     * Sets the value of the startDependentStageId property.
     * 
     */
    public void setStartDependentStageId(int value) {
        this.startDependentStageId = value;
    }

    /**
     * Gets the value of the startDependentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDependentDate() {
        return startDependentDate;
    }

    /**
     * Sets the value of the startDependentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDependentDate(String value) {
        this.startDependentDate = value;
    }

    /**
     * Gets the value of the startDependentTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDependentTime() {
        return startDependentTime;
    }

    /**
     * Sets the value of the startDependentTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDependentTime(String value) {
        this.startDependentTime = value;
    }

    /**
     * Gets the value of the stageLocking property.
     * 
     */
    public int getStageLocking() {
        return stageLocking;
    }

    /**
     * Sets the value of the stageLocking property.
     * 
     */
    public void setStageLocking(int value) {
        this.stageLocking = value;
    }

    /**
     * Gets the value of the stageOneDecisionOnly property.
     * 
     */
    public boolean isStageOneDecisionOnly() {
        return stageOneDecisionOnly;
    }

    /**
     * Sets the value of the stageOneDecisionOnly property.
     * 
     */
    public void setStageOneDecisionOnly(boolean value) {
        this.stageOneDecisionOnly = value;
    }

    /**
     * Gets the value of the stagePrivate property.
     * 
     */
    public boolean isStagePrivate() {
        return stagePrivate;
    }

    /**
     * Sets the value of the stagePrivate property.
     * 
     */
    public void setStagePrivate(boolean value) {
        this.stagePrivate = value;
    }

    /**
     * Gets the value of the deadlineCalculateOn property.
     * 
     */
    public int getDeadlineCalculateOn() {
        return deadlineCalculateOn;
    }

    /**
     * Sets the value of the deadlineCalculateOn property.
     * 
     */
    public void setDeadlineCalculateOn(int value) {
        this.deadlineCalculateOn = value;
    }

}
