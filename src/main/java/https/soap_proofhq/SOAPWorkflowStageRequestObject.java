
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowStageRequestObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowStageRequestObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadline_days" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="deadline_hours" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="deadline_minutes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_trigger" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_days" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_hours" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_minutes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_locking" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_one_decision_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stage_private" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stage_reviewers" type="{https://soap.proofhq.com/}SOAPWorkflowRecipientRequestObjectArray"/>
 *         &lt;element name="deadline_calculate_on" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowStageRequestObject", propOrder = {
    "stageId",
    "name",
    "deadlineDays",
    "deadlineHours",
    "deadlineMinutes",
    "startTrigger",
    "startDependentStageId",
    "startDependentDays",
    "startDependentHours",
    "startDependentMinutes",
    "stageLocking",
    "stageOneDecisionOnly",
    "stagePrivate",
    "stageReviewers",
    "deadlineCalculateOn"
})
public class SOAPWorkflowStageRequestObject {

    @XmlElement(name = "stage_id")
    protected int stageId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(name = "deadline_days")
    protected int deadlineDays;
    @XmlElement(name = "deadline_hours")
    protected int deadlineHours;
    @XmlElement(name = "deadline_minutes")
    protected int deadlineMinutes;
    @XmlElement(name = "start_trigger")
    protected int startTrigger;
    @XmlElement(name = "start_dependent_stage_id")
    protected int startDependentStageId;
    @XmlElement(name = "start_dependent_days")
    protected int startDependentDays;
    @XmlElement(name = "start_dependent_hours")
    protected int startDependentHours;
    @XmlElement(name = "start_dependent_minutes")
    protected int startDependentMinutes;
    @XmlElement(name = "stage_locking")
    protected int stageLocking;
    @XmlElement(name = "stage_one_decision_only")
    protected boolean stageOneDecisionOnly;
    @XmlElement(name = "stage_private")
    protected boolean stagePrivate;
    @XmlElement(name = "stage_reviewers", required = true)
    protected SOAPWorkflowRecipientRequestObjectArray stageReviewers;
    @XmlElement(name = "deadline_calculate_on")
    protected Integer deadlineCalculateOn;

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the deadlineDays property.
     * 
     */
    public int getDeadlineDays() {
        return deadlineDays;
    }

    /**
     * Sets the value of the deadlineDays property.
     * 
     */
    public void setDeadlineDays(int value) {
        this.deadlineDays = value;
    }

    /**
     * Gets the value of the deadlineHours property.
     * 
     */
    public int getDeadlineHours() {
        return deadlineHours;
    }

    /**
     * Sets the value of the deadlineHours property.
     * 
     */
    public void setDeadlineHours(int value) {
        this.deadlineHours = value;
    }

    /**
     * Gets the value of the deadlineMinutes property.
     * 
     */
    public int getDeadlineMinutes() {
        return deadlineMinutes;
    }

    /**
     * Sets the value of the deadlineMinutes property.
     * 
     */
    public void setDeadlineMinutes(int value) {
        this.deadlineMinutes = value;
    }

    /**
     * Gets the value of the startTrigger property.
     * 
     */
    public int getStartTrigger() {
        return startTrigger;
    }

    /**
     * Sets the value of the startTrigger property.
     * 
     */
    public void setStartTrigger(int value) {
        this.startTrigger = value;
    }

    /**
     * Gets the value of the startDependentStageId property.
     * 
     */
    public int getStartDependentStageId() {
        return startDependentStageId;
    }

    /**
     * Sets the value of the startDependentStageId property.
     * 
     */
    public void setStartDependentStageId(int value) {
        this.startDependentStageId = value;
    }

    /**
     * Gets the value of the startDependentDays property.
     * 
     */
    public int getStartDependentDays() {
        return startDependentDays;
    }

    /**
     * Sets the value of the startDependentDays property.
     * 
     */
    public void setStartDependentDays(int value) {
        this.startDependentDays = value;
    }

    /**
     * Gets the value of the startDependentHours property.
     * 
     */
    public int getStartDependentHours() {
        return startDependentHours;
    }

    /**
     * Sets the value of the startDependentHours property.
     * 
     */
    public void setStartDependentHours(int value) {
        this.startDependentHours = value;
    }

    /**
     * Gets the value of the startDependentMinutes property.
     * 
     */
    public int getStartDependentMinutes() {
        return startDependentMinutes;
    }

    /**
     * Sets the value of the startDependentMinutes property.
     * 
     */
    public void setStartDependentMinutes(int value) {
        this.startDependentMinutes = value;
    }

    /**
     * Gets the value of the stageLocking property.
     * 
     */
    public int getStageLocking() {
        return stageLocking;
    }

    /**
     * Sets the value of the stageLocking property.
     * 
     */
    public void setStageLocking(int value) {
        this.stageLocking = value;
    }

    /**
     * Gets the value of the stageOneDecisionOnly property.
     * 
     */
    public boolean isStageOneDecisionOnly() {
        return stageOneDecisionOnly;
    }

    /**
     * Sets the value of the stageOneDecisionOnly property.
     * 
     */
    public void setStageOneDecisionOnly(boolean value) {
        this.stageOneDecisionOnly = value;
    }

    /**
     * Gets the value of the stagePrivate property.
     * 
     */
    public boolean isStagePrivate() {
        return stagePrivate;
    }

    /**
     * Sets the value of the stagePrivate property.
     * 
     */
    public void setStagePrivate(boolean value) {
        this.stagePrivate = value;
    }

    /**
     * Gets the value of the stageReviewers property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPWorkflowRecipientRequestObjectArray }
     *     
     */
    public SOAPWorkflowRecipientRequestObjectArray getStageReviewers() {
        return stageReviewers;
    }

    /**
     * Sets the value of the stageReviewers property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPWorkflowRecipientRequestObjectArray }
     *     
     */
    public void setStageReviewers(SOAPWorkflowRecipientRequestObjectArray value) {
        this.stageReviewers = value;
    }

    /**
     * Gets the value of the deadlineCalculateOn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeadlineCalculateOn() {
        return deadlineCalculateOn;
    }

    /**
     * Sets the value of the deadlineCalculateOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeadlineCalculateOn(Integer value) {
        this.deadlineCalculateOn = value;
    }

}
