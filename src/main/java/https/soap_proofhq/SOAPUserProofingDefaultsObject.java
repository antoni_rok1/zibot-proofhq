
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPUserProofingDefaultsObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPUserProofingDefaultsObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="validate_subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="email_notifications" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="login_required" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="autoclose" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="one_decision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="block_download" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPUserProofingDefaultsObject", propOrder = {
    "subject",
    "message",
    "subscriptions",
    "defaultRole",
    "validateSubscriptions",
    "emailNotifications",
    "loginRequired",
    "autoclose",
    "oneDecision",
    "blockDownload"
})
public class SOAPUserProofingDefaultsObject {

    @XmlElement(required = true)
    protected String subject;
    @XmlElement(required = true)
    protected String message;
    protected boolean subscriptions;
    @XmlElement(name = "default_role", required = true)
    protected String defaultRole;
    @XmlElement(name = "validate_subscriptions")
    protected boolean validateSubscriptions;
    @XmlElement(name = "email_notifications", required = true)
    protected String emailNotifications;
    @XmlElement(name = "login_required")
    protected boolean loginRequired;
    protected boolean autoclose;
    @XmlElement(name = "one_decision")
    protected boolean oneDecision;
    @XmlElement(name = "block_download")
    protected boolean blockDownload;

    /**
     * Gets the value of the subject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the value of the subject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the subscriptions property.
     * 
     */
    public boolean isSubscriptions() {
        return subscriptions;
    }

    /**
     * Sets the value of the subscriptions property.
     * 
     */
    public void setSubscriptions(boolean value) {
        this.subscriptions = value;
    }

    /**
     * Gets the value of the defaultRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRole() {
        return defaultRole;
    }

    /**
     * Sets the value of the defaultRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRole(String value) {
        this.defaultRole = value;
    }

    /**
     * Gets the value of the validateSubscriptions property.
     * 
     */
    public boolean isValidateSubscriptions() {
        return validateSubscriptions;
    }

    /**
     * Sets the value of the validateSubscriptions property.
     * 
     */
    public void setValidateSubscriptions(boolean value) {
        this.validateSubscriptions = value;
    }

    /**
     * Gets the value of the emailNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailNotifications() {
        return emailNotifications;
    }

    /**
     * Sets the value of the emailNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailNotifications(String value) {
        this.emailNotifications = value;
    }

    /**
     * Gets the value of the loginRequired property.
     * 
     */
    public boolean isLoginRequired() {
        return loginRequired;
    }

    /**
     * Sets the value of the loginRequired property.
     * 
     */
    public void setLoginRequired(boolean value) {
        this.loginRequired = value;
    }

    /**
     * Gets the value of the autoclose property.
     * 
     */
    public boolean isAutoclose() {
        return autoclose;
    }

    /**
     * Sets the value of the autoclose property.
     * 
     */
    public void setAutoclose(boolean value) {
        this.autoclose = value;
    }

    /**
     * Gets the value of the oneDecision property.
     * 
     */
    public boolean isOneDecision() {
        return oneDecision;
    }

    /**
     * Sets the value of the oneDecision property.
     * 
     */
    public void setOneDecision(boolean value) {
        this.oneDecision = value;
    }

    /**
     * Gets the value of the blockDownload property.
     * 
     */
    public boolean isBlockDownload() {
        return blockDownload;
    }

    /**
     * Sets the value of the blockDownload property.
     * 
     */
    public void setBlockDownload(boolean value) {
        this.blockDownload = value;
    }

}
