
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPFileUploadObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPFileUploadObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filehash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="md5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorstring" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPFileUploadObject", propOrder = {
    "filehash",
    "filename",
    "md5",
    "errorcode",
    "errorstring"
})
public class SOAPFileUploadObject {

    @XmlElement(required = true)
    protected String filehash;
    @XmlElement(required = true)
    protected String filename;
    @XmlElement(required = true)
    protected String md5;
    @XmlElement(required = true)
    protected String errorcode;
    @XmlElement(required = true)
    protected String errorstring;

    /**
     * Gets the value of the filehash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilehash() {
        return filehash;
    }

    /**
     * Sets the value of the filehash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilehash(String value) {
        this.filehash = value;
    }

    /**
     * Gets the value of the filename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Sets the value of the filename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilename(String value) {
        this.filename = value;
    }

    /**
     * Gets the value of the md5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMd5() {
        return md5;
    }

    /**
     * Sets the value of the md5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMd5(String value) {
        this.md5 = value;
    }

    /**
     * Gets the value of the errorcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     * Sets the value of the errorcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorcode(String value) {
        this.errorcode = value;
    }

    /**
     * Gets the value of the errorstring property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorstring() {
        return errorstring;
    }

    /**
     * Sets the value of the errorstring property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorstring(String value) {
        this.errorstring = value;
    }

}
