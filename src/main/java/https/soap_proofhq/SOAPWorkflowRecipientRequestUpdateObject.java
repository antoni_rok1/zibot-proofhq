
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowRecipientRequestUpdateObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowRecipientRequestUpdateObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="recipient_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="email_notification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="primary_decision_maker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowRecipientRequestUpdateObject", propOrder = {
    "recipientId",
    "stageId",
    "role",
    "emailNotification",
    "primaryDecisionMaker"
})
public class SOAPWorkflowRecipientRequestUpdateObject {

    @XmlElement(name = "recipient_id")
    protected int recipientId;
    @XmlElement(name = "stage_id")
    protected int stageId;
    protected int role;
    @XmlElement(name = "email_notification")
    protected int emailNotification;
    @XmlElement(name = "primary_decision_maker")
    protected boolean primaryDecisionMaker;

    /**
     * Gets the value of the recipientId property.
     * 
     */
    public int getRecipientId() {
        return recipientId;
    }

    /**
     * Sets the value of the recipientId property.
     * 
     */
    public void setRecipientId(int value) {
        this.recipientId = value;
    }

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the role property.
     * 
     */
    public int getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     */
    public void setRole(int value) {
        this.role = value;
    }

    /**
     * Gets the value of the emailNotification property.
     * 
     */
    public int getEmailNotification() {
        return emailNotification;
    }

    /**
     * Sets the value of the emailNotification property.
     * 
     */
    public void setEmailNotification(int value) {
        this.emailNotification = value;
    }

    /**
     * Gets the value of the primaryDecisionMaker property.
     * 
     */
    public boolean isPrimaryDecisionMaker() {
        return primaryDecisionMaker;
    }

    /**
     * Sets the value of the primaryDecisionMaker property.
     * 
     */
    public void setPrimaryDecisionMaker(boolean value) {
        this.primaryDecisionMaker = value;
    }

}
