
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowProofRecipientRequestUpdateObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowProofRecipientRequestUpdateObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="recipient_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="position" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="email_notification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="primary_decision_maker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowProofRecipientRequestUpdateObject", propOrder = {
    "recipientId",
    "stageId",
    "name",
    "position",
    "decision",
    "role",
    "emailNotification",
    "primaryDecisionMaker"
})
public class SOAPWorkflowProofRecipientRequestUpdateObject {

    @XmlElement(name = "recipient_id")
    protected int recipientId;
    @XmlElement(name = "stage_id")
    protected int stageId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String position;
    @XmlElement(required = true)
    protected String decision;
    protected int role;
    @XmlElement(name = "email_notification")
    protected int emailNotification;
    @XmlElement(name = "primary_decision_maker")
    protected boolean primaryDecisionMaker;

    /**
     * Gets the value of the recipientId property.
     * 
     */
    public int getRecipientId() {
        return recipientId;
    }

    /**
     * Sets the value of the recipientId property.
     * 
     */
    public void setRecipientId(int value) {
        this.recipientId = value;
    }

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the role property.
     * 
     */
    public int getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     */
    public void setRole(int value) {
        this.role = value;
    }

    /**
     * Gets the value of the emailNotification property.
     * 
     */
    public int getEmailNotification() {
        return emailNotification;
    }

    /**
     * Sets the value of the emailNotification property.
     * 
     */
    public void setEmailNotification(int value) {
        this.emailNotification = value;
    }

    /**
     * Gets the value of the primaryDecisionMaker property.
     * 
     */
    public boolean isPrimaryDecisionMaker() {
        return primaryDecisionMaker;
    }

    /**
     * Sets the value of the primaryDecisionMaker property.
     * 
     */
    public void setPrimaryDecisionMaker(boolean value) {
        this.primaryDecisionMaker = value;
    }

}
