
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPUserObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPUserObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="openid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="first_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="last_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="position" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="permissions" type="{https://soap.proofhq.com/}SOAPPermissionsObject"/>
 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_timezone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_format" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="proofing_defaults" type="{https://soap.proofhq.com/}SOAPUserProofingDefaultsObject"/>
 *         &lt;element name="email_aliases" type="{https://soap.proofhq.com/}SOAPUserEmailObjectArray"/>
 *         &lt;element name="product_marketing_emails" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="api_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="send_proof_ready_email" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="api_token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPUserObject", propOrder = {
    "id",
    "email",
    "openid",
    "firstName",
    "lastName",
    "position",
    "status",
    "permissions",
    "street",
    "city",
    "state",
    "postcode",
    "country",
    "telephone",
    "mobile",
    "fax",
    "dateTimezone",
    "dateFormat",
    "language",
    "proofingDefaults",
    "emailAliases",
    "productMarketingEmails",
    "apiOnly",
    "sendProofReadyEmail",
    "apiToken"
})
public class SOAPUserObject {

    protected int id;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(required = true)
    protected String openid;
    @XmlElement(name = "first_name", required = true)
    protected String firstName;
    @XmlElement(name = "last_name", required = true)
    protected String lastName;
    @XmlElement(required = true)
    protected String position;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected SOAPPermissionsObject permissions;
    @XmlElement(required = true)
    protected String street;
    @XmlElement(required = true)
    protected String city;
    @XmlElement(required = true)
    protected String state;
    @XmlElement(required = true)
    protected String postcode;
    @XmlElement(required = true)
    protected String country;
    @XmlElement(required = true)
    protected String telephone;
    @XmlElement(required = true)
    protected String mobile;
    @XmlElement(required = true)
    protected String fax;
    @XmlElement(name = "date_timezone", required = true)
    protected String dateTimezone;
    @XmlElement(name = "date_format", required = true)
    protected String dateFormat;
    @XmlElement(required = true)
    protected String language;
    @XmlElement(name = "proofing_defaults", required = true)
    protected SOAPUserProofingDefaultsObject proofingDefaults;
    @XmlElement(name = "email_aliases", required = true)
    protected SOAPUserEmailObjectArray emailAliases;
    @XmlElement(name = "product_marketing_emails")
    protected boolean productMarketingEmails;
    @XmlElement(name = "api_only")
    protected boolean apiOnly;
    @XmlElement(name = "send_proof_ready_email")
    protected boolean sendProofReadyEmail;
    @XmlElement(name = "api_token", required = true)
    protected String apiToken;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the openid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenid() {
        return openid;
    }

    /**
     * Sets the value of the openid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenid(String value) {
        this.openid = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the permissions property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPPermissionsObject }
     *     
     */
    public SOAPPermissionsObject getPermissions() {
        return permissions;
    }

    /**
     * Sets the value of the permissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPPermissionsObject }
     *     
     */
    public void setPermissions(SOAPPermissionsObject value) {
        this.permissions = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the postcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the telephone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the value of the telephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephone(String value) {
        this.telephone = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the dateTimezone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTimezone() {
        return dateTimezone;
    }

    /**
     * Sets the value of the dateTimezone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTimezone(String value) {
        this.dateTimezone = value;
    }

    /**
     * Gets the value of the dateFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets the value of the dateFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFormat(String value) {
        this.dateFormat = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the proofingDefaults property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPUserProofingDefaultsObject }
     *     
     */
    public SOAPUserProofingDefaultsObject getProofingDefaults() {
        return proofingDefaults;
    }

    /**
     * Sets the value of the proofingDefaults property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPUserProofingDefaultsObject }
     *     
     */
    public void setProofingDefaults(SOAPUserProofingDefaultsObject value) {
        this.proofingDefaults = value;
    }

    /**
     * Gets the value of the emailAliases property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPUserEmailObjectArray }
     *     
     */
    public SOAPUserEmailObjectArray getEmailAliases() {
        return emailAliases;
    }

    /**
     * Sets the value of the emailAliases property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPUserEmailObjectArray }
     *     
     */
    public void setEmailAliases(SOAPUserEmailObjectArray value) {
        this.emailAliases = value;
    }

    /**
     * Gets the value of the productMarketingEmails property.
     * 
     */
    public boolean isProductMarketingEmails() {
        return productMarketingEmails;
    }

    /**
     * Sets the value of the productMarketingEmails property.
     * 
     */
    public void setProductMarketingEmails(boolean value) {
        this.productMarketingEmails = value;
    }

    /**
     * Gets the value of the apiOnly property.
     * 
     */
    public boolean isApiOnly() {
        return apiOnly;
    }

    /**
     * Sets the value of the apiOnly property.
     * 
     */
    public void setApiOnly(boolean value) {
        this.apiOnly = value;
    }

    /**
     * Gets the value of the sendProofReadyEmail property.
     * 
     */
    public boolean isSendProofReadyEmail() {
        return sendProofReadyEmail;
    }

    /**
     * Sets the value of the sendProofReadyEmail property.
     * 
     */
    public void setSendProofReadyEmail(boolean value) {
        this.sendProofReadyEmail = value;
    }

    /**
     * Gets the value of the apiToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiToken() {
        return apiToken;
    }

    /**
     * Sets the value of the apiToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiToken(String value) {
        this.apiToken = value;
    }

}
