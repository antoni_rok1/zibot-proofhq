
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPLoginObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPLoginObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="session" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="user_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="user_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="user_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="organisation_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="organisation_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="limit_storage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="limit_proofs" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="max_file_size" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="default_subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="default_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="default_upload_host" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="default_email_notifications" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="default_authorized_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_enable_subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_validate_subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_auto_lock" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_lock_after_first_decision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_enable_download" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_enable_team_url" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_enable_embed_player" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_dashboard_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_workspace_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_details_page_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_versions_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_quick_start_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_user_guide_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_welcome_screen_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_forum_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="default_show_help_blog_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="can_create_folder" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="can_create_private_folder" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPLoginObject", propOrder = {
    "session",
    "userId",
    "userName",
    "userEmail",
    "organisationId",
    "organisationName",
    "limitStorage",
    "limitProofs",
    "maxFileSize",
    "defaultSubject",
    "defaultMessage",
    "defaultUploadHost",
    "defaultEmailNotifications",
    "defaultAuthorizedOnly",
    "defaultEnableSubscriptions",
    "defaultValidateSubscriptions",
    "defaultAutoLock",
    "defaultLockAfterFirstDecision",
    "defaultEnableDownload",
    "defaultEnableTeamUrl",
    "defaultEnableEmbedPlayer",
    "defaultShowDashboardLink",
    "defaultShowWorkspaceLink",
    "defaultShowDetailsPageLink",
    "defaultShowVersionsLink",
    "defaultShowHelpLink",
    "defaultShowHelpQuickStartLink",
    "defaultShowHelpUserGuideLink",
    "defaultShowHelpWelcomeScreenLink",
    "defaultShowHelpForumLink",
    "defaultShowHelpBlogLink",
    "domain",
    "canCreateFolder",
    "canCreatePrivateFolder"
})
public class SOAPLoginObject {

    @XmlElement(required = true)
    protected String session;
    @XmlElement(name = "user_id")
    protected int userId;
    @XmlElement(name = "user_name", required = true)
    protected String userName;
    @XmlElement(name = "user_email", required = true)
    protected String userEmail;
    @XmlElement(name = "organisation_id")
    protected int organisationId;
    @XmlElement(name = "organisation_name", required = true)
    protected String organisationName;
    @XmlElement(name = "limit_storage")
    protected int limitStorage;
    @XmlElement(name = "limit_proofs")
    protected int limitProofs;
    @XmlElement(name = "max_file_size")
    protected int maxFileSize;
    @XmlElement(name = "default_subject", required = true)
    protected String defaultSubject;
    @XmlElement(name = "default_message", required = true)
    protected String defaultMessage;
    @XmlElement(name = "default_upload_host", required = true)
    protected String defaultUploadHost;
    @XmlElement(name = "default_email_notifications", required = true)
    protected String defaultEmailNotifications;
    @XmlElement(name = "default_authorized_only")
    protected boolean defaultAuthorizedOnly;
    @XmlElement(name = "default_enable_subscriptions")
    protected boolean defaultEnableSubscriptions;
    @XmlElement(name = "default_validate_subscriptions")
    protected boolean defaultValidateSubscriptions;
    @XmlElement(name = "default_auto_lock")
    protected boolean defaultAutoLock;
    @XmlElement(name = "default_lock_after_first_decision")
    protected boolean defaultLockAfterFirstDecision;
    @XmlElement(name = "default_enable_download")
    protected boolean defaultEnableDownload;
    @XmlElement(name = "default_enable_team_url")
    protected boolean defaultEnableTeamUrl;
    @XmlElement(name = "default_enable_embed_player")
    protected boolean defaultEnableEmbedPlayer;
    @XmlElement(name = "default_show_dashboard_link")
    protected boolean defaultShowDashboardLink;
    @XmlElement(name = "default_show_workspace_link")
    protected boolean defaultShowWorkspaceLink;
    @XmlElement(name = "default_show_details_page_link")
    protected boolean defaultShowDetailsPageLink;
    @XmlElement(name = "default_show_versions_link")
    protected boolean defaultShowVersionsLink;
    @XmlElement(name = "default_show_help_link")
    protected boolean defaultShowHelpLink;
    @XmlElement(name = "default_show_help_quick_start_link")
    protected boolean defaultShowHelpQuickStartLink;
    @XmlElement(name = "default_show_help_user_guide_link")
    protected boolean defaultShowHelpUserGuideLink;
    @XmlElement(name = "default_show_help_welcome_screen_link")
    protected boolean defaultShowHelpWelcomeScreenLink;
    @XmlElement(name = "default_show_help_forum_link")
    protected boolean defaultShowHelpForumLink;
    @XmlElement(name = "default_show_help_blog_link")
    protected boolean defaultShowHelpBlogLink;
    @XmlElement(required = true)
    protected String domain;
    @XmlElement(name = "can_create_folder")
    protected boolean canCreateFolder;
    @XmlElement(name = "can_create_private_folder")
    protected boolean canCreatePrivateFolder;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSession(String value) {
        this.session = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(int value) {
        this.userId = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the userEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets the value of the userEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserEmail(String value) {
        this.userEmail = value;
    }

    /**
     * Gets the value of the organisationId property.
     * 
     */
    public int getOrganisationId() {
        return organisationId;
    }

    /**
     * Sets the value of the organisationId property.
     * 
     */
    public void setOrganisationId(int value) {
        this.organisationId = value;
    }

    /**
     * Gets the value of the organisationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisationName() {
        return organisationName;
    }

    /**
     * Sets the value of the organisationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisationName(String value) {
        this.organisationName = value;
    }

    /**
     * Gets the value of the limitStorage property.
     * 
     */
    public int getLimitStorage() {
        return limitStorage;
    }

    /**
     * Sets the value of the limitStorage property.
     * 
     */
    public void setLimitStorage(int value) {
        this.limitStorage = value;
    }

    /**
     * Gets the value of the limitProofs property.
     * 
     */
    public int getLimitProofs() {
        return limitProofs;
    }

    /**
     * Sets the value of the limitProofs property.
     * 
     */
    public void setLimitProofs(int value) {
        this.limitProofs = value;
    }

    /**
     * Gets the value of the maxFileSize property.
     * 
     */
    public int getMaxFileSize() {
        return maxFileSize;
    }

    /**
     * Sets the value of the maxFileSize property.
     * 
     */
    public void setMaxFileSize(int value) {
        this.maxFileSize = value;
    }

    /**
     * Gets the value of the defaultSubject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultSubject() {
        return defaultSubject;
    }

    /**
     * Sets the value of the defaultSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultSubject(String value) {
        this.defaultSubject = value;
    }

    /**
     * Gets the value of the defaultMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultMessage() {
        return defaultMessage;
    }

    /**
     * Sets the value of the defaultMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultMessage(String value) {
        this.defaultMessage = value;
    }

    /**
     * Gets the value of the defaultUploadHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultUploadHost() {
        return defaultUploadHost;
    }

    /**
     * Sets the value of the defaultUploadHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultUploadHost(String value) {
        this.defaultUploadHost = value;
    }

    /**
     * Gets the value of the defaultEmailNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultEmailNotifications() {
        return defaultEmailNotifications;
    }

    /**
     * Sets the value of the defaultEmailNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultEmailNotifications(String value) {
        this.defaultEmailNotifications = value;
    }

    /**
     * Gets the value of the defaultAuthorizedOnly property.
     * 
     */
    public boolean isDefaultAuthorizedOnly() {
        return defaultAuthorizedOnly;
    }

    /**
     * Sets the value of the defaultAuthorizedOnly property.
     * 
     */
    public void setDefaultAuthorizedOnly(boolean value) {
        this.defaultAuthorizedOnly = value;
    }

    /**
     * Gets the value of the defaultEnableSubscriptions property.
     * 
     */
    public boolean isDefaultEnableSubscriptions() {
        return defaultEnableSubscriptions;
    }

    /**
     * Sets the value of the defaultEnableSubscriptions property.
     * 
     */
    public void setDefaultEnableSubscriptions(boolean value) {
        this.defaultEnableSubscriptions = value;
    }

    /**
     * Gets the value of the defaultValidateSubscriptions property.
     * 
     */
    public boolean isDefaultValidateSubscriptions() {
        return defaultValidateSubscriptions;
    }

    /**
     * Sets the value of the defaultValidateSubscriptions property.
     * 
     */
    public void setDefaultValidateSubscriptions(boolean value) {
        this.defaultValidateSubscriptions = value;
    }

    /**
     * Gets the value of the defaultAutoLock property.
     * 
     */
    public boolean isDefaultAutoLock() {
        return defaultAutoLock;
    }

    /**
     * Sets the value of the defaultAutoLock property.
     * 
     */
    public void setDefaultAutoLock(boolean value) {
        this.defaultAutoLock = value;
    }

    /**
     * Gets the value of the defaultLockAfterFirstDecision property.
     * 
     */
    public boolean isDefaultLockAfterFirstDecision() {
        return defaultLockAfterFirstDecision;
    }

    /**
     * Sets the value of the defaultLockAfterFirstDecision property.
     * 
     */
    public void setDefaultLockAfterFirstDecision(boolean value) {
        this.defaultLockAfterFirstDecision = value;
    }

    /**
     * Gets the value of the defaultEnableDownload property.
     * 
     */
    public boolean isDefaultEnableDownload() {
        return defaultEnableDownload;
    }

    /**
     * Sets the value of the defaultEnableDownload property.
     * 
     */
    public void setDefaultEnableDownload(boolean value) {
        this.defaultEnableDownload = value;
    }

    /**
     * Gets the value of the defaultEnableTeamUrl property.
     * 
     */
    public boolean isDefaultEnableTeamUrl() {
        return defaultEnableTeamUrl;
    }

    /**
     * Sets the value of the defaultEnableTeamUrl property.
     * 
     */
    public void setDefaultEnableTeamUrl(boolean value) {
        this.defaultEnableTeamUrl = value;
    }

    /**
     * Gets the value of the defaultEnableEmbedPlayer property.
     * 
     */
    public boolean isDefaultEnableEmbedPlayer() {
        return defaultEnableEmbedPlayer;
    }

    /**
     * Sets the value of the defaultEnableEmbedPlayer property.
     * 
     */
    public void setDefaultEnableEmbedPlayer(boolean value) {
        this.defaultEnableEmbedPlayer = value;
    }

    /**
     * Gets the value of the defaultShowDashboardLink property.
     * 
     */
    public boolean isDefaultShowDashboardLink() {
        return defaultShowDashboardLink;
    }

    /**
     * Sets the value of the defaultShowDashboardLink property.
     * 
     */
    public void setDefaultShowDashboardLink(boolean value) {
        this.defaultShowDashboardLink = value;
    }

    /**
     * Gets the value of the defaultShowWorkspaceLink property.
     * 
     */
    public boolean isDefaultShowWorkspaceLink() {
        return defaultShowWorkspaceLink;
    }

    /**
     * Sets the value of the defaultShowWorkspaceLink property.
     * 
     */
    public void setDefaultShowWorkspaceLink(boolean value) {
        this.defaultShowWorkspaceLink = value;
    }

    /**
     * Gets the value of the defaultShowDetailsPageLink property.
     * 
     */
    public boolean isDefaultShowDetailsPageLink() {
        return defaultShowDetailsPageLink;
    }

    /**
     * Sets the value of the defaultShowDetailsPageLink property.
     * 
     */
    public void setDefaultShowDetailsPageLink(boolean value) {
        this.defaultShowDetailsPageLink = value;
    }

    /**
     * Gets the value of the defaultShowVersionsLink property.
     * 
     */
    public boolean isDefaultShowVersionsLink() {
        return defaultShowVersionsLink;
    }

    /**
     * Sets the value of the defaultShowVersionsLink property.
     * 
     */
    public void setDefaultShowVersionsLink(boolean value) {
        this.defaultShowVersionsLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpLink property.
     * 
     */
    public boolean isDefaultShowHelpLink() {
        return defaultShowHelpLink;
    }

    /**
     * Sets the value of the defaultShowHelpLink property.
     * 
     */
    public void setDefaultShowHelpLink(boolean value) {
        this.defaultShowHelpLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpQuickStartLink property.
     * 
     */
    public boolean isDefaultShowHelpQuickStartLink() {
        return defaultShowHelpQuickStartLink;
    }

    /**
     * Sets the value of the defaultShowHelpQuickStartLink property.
     * 
     */
    public void setDefaultShowHelpQuickStartLink(boolean value) {
        this.defaultShowHelpQuickStartLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpUserGuideLink property.
     * 
     */
    public boolean isDefaultShowHelpUserGuideLink() {
        return defaultShowHelpUserGuideLink;
    }

    /**
     * Sets the value of the defaultShowHelpUserGuideLink property.
     * 
     */
    public void setDefaultShowHelpUserGuideLink(boolean value) {
        this.defaultShowHelpUserGuideLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpWelcomeScreenLink property.
     * 
     */
    public boolean isDefaultShowHelpWelcomeScreenLink() {
        return defaultShowHelpWelcomeScreenLink;
    }

    /**
     * Sets the value of the defaultShowHelpWelcomeScreenLink property.
     * 
     */
    public void setDefaultShowHelpWelcomeScreenLink(boolean value) {
        this.defaultShowHelpWelcomeScreenLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpForumLink property.
     * 
     */
    public boolean isDefaultShowHelpForumLink() {
        return defaultShowHelpForumLink;
    }

    /**
     * Sets the value of the defaultShowHelpForumLink property.
     * 
     */
    public void setDefaultShowHelpForumLink(boolean value) {
        this.defaultShowHelpForumLink = value;
    }

    /**
     * Gets the value of the defaultShowHelpBlogLink property.
     * 
     */
    public boolean isDefaultShowHelpBlogLink() {
        return defaultShowHelpBlogLink;
    }

    /**
     * Sets the value of the defaultShowHelpBlogLink property.
     * 
     */
    public void setDefaultShowHelpBlogLink(boolean value) {
        this.defaultShowHelpBlogLink = value;
    }

    /**
     * Gets the value of the domain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Sets the value of the domain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomain(String value) {
        this.domain = value;
    }

    /**
     * Gets the value of the canCreateFolder property.
     * 
     */
    public boolean isCanCreateFolder() {
        return canCreateFolder;
    }

    /**
     * Sets the value of the canCreateFolder property.
     * 
     */
    public void setCanCreateFolder(boolean value) {
        this.canCreateFolder = value;
    }

    /**
     * Gets the value of the canCreatePrivateFolder property.
     * 
     */
    public boolean isCanCreatePrivateFolder() {
        return canCreatePrivateFolder;
    }

    /**
     * Sets the value of the canCreatePrivateFolder property.
     * 
     */
    public void setCanCreatePrivateFolder(boolean value) {
        this.canCreatePrivateFolder = value;
    }

}
