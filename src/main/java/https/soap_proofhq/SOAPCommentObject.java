
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPCommentObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPCommentObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="reviewer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="created_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="replies_count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="replies" type="{https://soap.proofhq.com/}SOAPCommentReplyObjectArray"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="drawing_xml" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPCommentObject", propOrder = {
    "id",
    "reviewerId",
    "author",
    "comment",
    "createdAt",
    "repliesCount",
    "replies",
    "url",
    "drawingXml",
    "location"
})
public class SOAPCommentObject {

    protected int id;
    @XmlElement(name = "reviewer_id")
    protected int reviewerId;
    @XmlElement(required = true)
    protected String author;
    @XmlElement(required = true)
    protected String comment;
    @XmlElement(name = "created_at", required = true)
    protected String createdAt;
    @XmlElement(name = "replies_count")
    protected int repliesCount;
    @XmlElement(required = true)
    protected SOAPCommentReplyObjectArray replies;
    @XmlElement(required = true)
    protected String url;
    @XmlElement(name = "drawing_xml", required = true)
    protected String drawingXml;
    @XmlElement(required = true)
    protected String location;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the reviewerId property.
     * 
     */
    public int getReviewerId() {
        return reviewerId;
    }

    /**
     * Sets the value of the reviewerId property.
     * 
     */
    public void setReviewerId(int value) {
        this.reviewerId = value;
    }

    /**
     * Gets the value of the author property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthor(String value) {
        this.author = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the createdAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the value of the createdAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedAt(String value) {
        this.createdAt = value;
    }

    /**
     * Gets the value of the repliesCount property.
     * 
     */
    public int getRepliesCount() {
        return repliesCount;
    }

    /**
     * Sets the value of the repliesCount property.
     * 
     */
    public void setRepliesCount(int value) {
        this.repliesCount = value;
    }

    /**
     * Gets the value of the replies property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPCommentReplyObjectArray }
     *     
     */
    public SOAPCommentReplyObjectArray getReplies() {
        return replies;
    }

    /**
     * Sets the value of the replies property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPCommentReplyObjectArray }
     *     
     */
    public void setReplies(SOAPCommentReplyObjectArray value) {
        this.replies = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the drawingXml property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrawingXml() {
        return drawingXml;
    }

    /**
     * Sets the value of the drawingXml property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrawingXml(String value) {
        this.drawingXml = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

}
