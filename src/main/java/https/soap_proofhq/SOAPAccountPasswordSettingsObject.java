
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPAccountPasswordSettingsObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPAccountPasswordSettingsObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="password_length" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lcase_characters" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ucase_characters" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeric_characters" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="symbol_characters" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="characters_repetition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="password_lifetime" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="password_repetition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="bruteforce_attempts" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="bruteforce_lock_time" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPAccountPasswordSettingsObject", propOrder = {
    "passwordLength",
    "lcaseCharacters",
    "ucaseCharacters",
    "numericCharacters",
    "symbolCharacters",
    "charactersRepetition",
    "passwordLifetime",
    "passwordRepetition",
    "bruteforceAttempts",
    "bruteforceLockTime"
})
public class SOAPAccountPasswordSettingsObject {

    @XmlElement(name = "password_length")
    protected int passwordLength;
    @XmlElement(name = "lcase_characters")
    protected int lcaseCharacters;
    @XmlElement(name = "ucase_characters")
    protected int ucaseCharacters;
    @XmlElement(name = "numeric_characters")
    protected int numericCharacters;
    @XmlElement(name = "symbol_characters")
    protected int symbolCharacters;
    @XmlElement(name = "characters_repetition")
    protected int charactersRepetition;
    @XmlElement(name = "password_lifetime")
    protected int passwordLifetime;
    @XmlElement(name = "password_repetition")
    protected int passwordRepetition;
    @XmlElement(name = "bruteforce_attempts")
    protected int bruteforceAttempts;
    @XmlElement(name = "bruteforce_lock_time")
    protected int bruteforceLockTime;

    /**
     * Gets the value of the passwordLength property.
     * 
     */
    public int getPasswordLength() {
        return passwordLength;
    }

    /**
     * Sets the value of the passwordLength property.
     * 
     */
    public void setPasswordLength(int value) {
        this.passwordLength = value;
    }

    /**
     * Gets the value of the lcaseCharacters property.
     * 
     */
    public int getLcaseCharacters() {
        return lcaseCharacters;
    }

    /**
     * Sets the value of the lcaseCharacters property.
     * 
     */
    public void setLcaseCharacters(int value) {
        this.lcaseCharacters = value;
    }

    /**
     * Gets the value of the ucaseCharacters property.
     * 
     */
    public int getUcaseCharacters() {
        return ucaseCharacters;
    }

    /**
     * Sets the value of the ucaseCharacters property.
     * 
     */
    public void setUcaseCharacters(int value) {
        this.ucaseCharacters = value;
    }

    /**
     * Gets the value of the numericCharacters property.
     * 
     */
    public int getNumericCharacters() {
        return numericCharacters;
    }

    /**
     * Sets the value of the numericCharacters property.
     * 
     */
    public void setNumericCharacters(int value) {
        this.numericCharacters = value;
    }

    /**
     * Gets the value of the symbolCharacters property.
     * 
     */
    public int getSymbolCharacters() {
        return symbolCharacters;
    }

    /**
     * Sets the value of the symbolCharacters property.
     * 
     */
    public void setSymbolCharacters(int value) {
        this.symbolCharacters = value;
    }

    /**
     * Gets the value of the charactersRepetition property.
     * 
     */
    public int getCharactersRepetition() {
        return charactersRepetition;
    }

    /**
     * Sets the value of the charactersRepetition property.
     * 
     */
    public void setCharactersRepetition(int value) {
        this.charactersRepetition = value;
    }

    /**
     * Gets the value of the passwordLifetime property.
     * 
     */
    public int getPasswordLifetime() {
        return passwordLifetime;
    }

    /**
     * Sets the value of the passwordLifetime property.
     * 
     */
    public void setPasswordLifetime(int value) {
        this.passwordLifetime = value;
    }

    /**
     * Gets the value of the passwordRepetition property.
     * 
     */
    public int getPasswordRepetition() {
        return passwordRepetition;
    }

    /**
     * Sets the value of the passwordRepetition property.
     * 
     */
    public void setPasswordRepetition(int value) {
        this.passwordRepetition = value;
    }

    /**
     * Gets the value of the bruteforceAttempts property.
     * 
     */
    public int getBruteforceAttempts() {
        return bruteforceAttempts;
    }

    /**
     * Sets the value of the bruteforceAttempts property.
     * 
     */
    public void setBruteforceAttempts(int value) {
        this.bruteforceAttempts = value;
    }

    /**
     * Gets the value of the bruteforceLockTime property.
     * 
     */
    public int getBruteforceLockTime() {
        return bruteforceLockTime;
    }

    /**
     * Sets the value of the bruteforceLockTime property.
     * 
     */
    public void setBruteforceLockTime(int value) {
        this.bruteforceLockTime = value;
    }

}
