
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPProgressObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPProgressObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sent" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="opened" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="comment_made" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="decision_made" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPProgressObject", propOrder = {
    "sent",
    "opened",
    "commentMade",
    "decisionMade"
})
public class SOAPProgressObject {

    protected int sent;
    protected int opened;
    @XmlElement(name = "comment_made")
    protected int commentMade;
    @XmlElement(name = "decision_made")
    protected int decisionMade;

    /**
     * Gets the value of the sent property.
     * 
     */
    public int getSent() {
        return sent;
    }

    /**
     * Sets the value of the sent property.
     * 
     */
    public void setSent(int value) {
        this.sent = value;
    }

    /**
     * Gets the value of the opened property.
     * 
     */
    public int getOpened() {
        return opened;
    }

    /**
     * Sets the value of the opened property.
     * 
     */
    public void setOpened(int value) {
        this.opened = value;
    }

    /**
     * Gets the value of the commentMade property.
     * 
     */
    public int getCommentMade() {
        return commentMade;
    }

    /**
     * Sets the value of the commentMade property.
     * 
     */
    public void setCommentMade(int value) {
        this.commentMade = value;
    }

    /**
     * Gets the value of the decisionMade property.
     * 
     */
    public int getDecisionMade() {
        return decisionMade;
    }

    /**
     * Sets the value of the decisionMade property.
     * 
     */
    public void setDecisionMade(int value) {
        this.decisionMade = value;
    }

}
