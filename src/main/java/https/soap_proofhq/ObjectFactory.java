
package https.soap_proofhq;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.soap_proofhq package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SOAPUserObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPUserObjectArrayElement");
    private final static QName _IntArrayElement_QNAME = new QName("https://soap.proofhq.com/", "IntArrayElement");
    private final static QName _SOAPFileListViewObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileListViewObjectArrayElement");
    private final static QName _SOAPCommentObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCommentObjectElement");
    private final static QName _SOAPPermissionsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPPermissionsObjectElement");
    private final static QName _SOAPFileListViewObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileListViewObjectElement");
    private final static QName _SOAPWorkflowProofRecipientRequestUpdateObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofRecipientRequestUpdateObjectElement");
    private final static QName _SOAPFileLinkObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileLinkObjectArrayElement");
    private final static QName _SOAPFileRecipientObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileRecipientObjectArrayElement");
    private final static QName _SOAPGroupObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPGroupObjectArrayElement");
    private final static QName _SOAPProofStageObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPProofStageObjectArrayElement");
    private final static QName _SOAPWorkflowProofStageRequestObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofStageRequestObjectElement");
    private final static QName _SOAPImageObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPImageObjectElement");
    private final static QName _SOAPCustomFieldObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCustomFieldObjectElement");
    private final static QName _SOAPSectionObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPSectionObjectElement");
    private final static QName _SOAPWorkflowStageRequestObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowStageRequestObjectElement");
    private final static QName _SOAPWorkflowProofStageObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofStageObjectArrayElement");
    private final static QName _SOAPSatelliteAccountObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPSatelliteAccountObjectElement");
    private final static QName _SOAPGroupMemberObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPGroupMemberObjectElement");
    private final static QName _SOAPWorkflowProofRecipientObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofRecipientObjectArrayElement");
    private final static QName _SOAPWorkflowStageObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowStageObjectArrayElement");
    private final static QName _SOAPAccountPasswordSettingsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPAccountPasswordSettingsObjectElement");
    private final static QName _SOAPFileUploadObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileUploadObjectElement");
    private final static QName _SOAPFileListViewCollectionObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileListViewCollectionObjectElement");
    private final static QName _SOAPWorkflowTemplateBasicObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowTemplateBasicObjectElement");
    private final static QName _SOAPWorkflowProofObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofObjectElement");
    private final static QName _SOAPFileActivityObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileActivityObjectArrayElement");
    private final static QName _SOAPCommentReplyObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCommentReplyObjectArrayElement");
    private final static QName _SOAPTagArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPTagArrayElement");
    private final static QName _SOAPDecisionReasonObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPDecisionReasonObjectElement");
    private final static QName _SOAPWorkflowRecipientRequestObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowRecipientRequestObjectElement");
    private final static QName _SOAPCustomFieldObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCustomFieldObjectArrayElement");
    private final static QName _SOAPProgressObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPProgressObjectElement");
    private final static QName _SOAPPlanObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPPlanObjectArrayElement");
    private final static QName _SOAPRecipientDecisionReasonObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPRecipientDecisionReasonObjectElement");
    private final static QName _SOAPCustomFieldOptionObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCustomFieldOptionObjectArrayElement");
    private final static QName _SOAPFileSettingsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileSettingsObjectElement");
    private final static QName _SOAPFileLinkObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileLinkObjectElement");
    private final static QName _SOAPPlanObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPPlanObjectElement");
    private final static QName _SOAPSectionObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPSectionObjectArrayElement");
    private final static QName _SOAPWorkflowProofObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofObjectArrayElement");
    private final static QName _SOAPWorkflowProofStageRequestUpdateObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofStageRequestUpdateObjectElement");
    private final static QName _SOAPSatelliteAccountObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPSatelliteAccountObjectArrayElement");
    private final static QName _SOAPWorkflowProofRecipientRequestObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofRecipientRequestObjectArrayElement");
    private final static QName _SOAPRecipientDecisionReasonObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPRecipientDecisionReasonObjectArrayElement");
    private final static QName _SOAPRecipientObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPRecipientObjectElement");
    private final static QName _SOAPRecipientObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPRecipientObjectArrayElement");
    private final static QName _SOAPActionObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPActionObjectElement");
    private final static QName _SOAPDecisionReasonObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPDecisionReasonObjectArrayElement");
    private final static QName _SOAPWorkflowProofStageObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofStageObjectElement");
    private final static QName _SOAPContactAccountObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactAccountObjectElement");
    private final static QName _SOAPFileEmbedCodeObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileEmbedCodeObjectElement");
    private final static QName _SOAPWorkspaceObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkspaceObjectArrayElement");
    private final static QName _AnyTypeArrayElement_QNAME = new QName("https://soap.proofhq.com/", "AnyTypeArrayElement");
    private final static QName _SOAPContactProofingDefaultsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactProofingDefaultsObjectElement");
    private final static QName _SOAPContactObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactObjectArrayElement");
    private final static QName _SOAPCommentObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCommentObjectArrayElement");
    private final static QName _SOAPWorkspaceObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkspaceObjectElement");
    private final static QName _SOAPWorkflowStageObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowStageObjectElement");
    private final static QName _SOAPDecisionObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPDecisionObjectArrayElement");
    private final static QName _SOAPGroupMemberObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPGroupMemberObjectArrayElement");
    private final static QName _SOAPFileRecipientObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileRecipientObjectElement");
    private final static QName _SOAPFileObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileObjectArrayElement");
    private final static QName _SOAPWorkflowRecipientObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowRecipientObjectElement");
    private final static QName _SOAPMembershipObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPMembershipObjectElement");
    private final static QName _SOAPFileObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileObjectElement");
    private final static QName _SOAPProofStageObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPProofStageObjectElement");
    private final static QName _SOAPWorkflowStageRequestUpdateObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowStageRequestUpdateObjectElement");
    private final static QName _SOAPGroupObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPGroupObjectElement");
    private final static QName _SOAPContactGroupObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactGroupObjectArrayElement");
    private final static QName _SOAPProfileObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPProfileObjectElement");
    private final static QName _SOAPLoginObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPLoginObjectElement");
    private final static QName _SOAPWorkflowTemplateObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowTemplateObjectElement");
    private final static QName _SOAPContactGroupObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactGroupObjectElement");
    private final static QName _SOAPWorkflowStageRequestObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowStageRequestObjectArrayElement");
    private final static QName _SOAPUserObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPUserObjectElement");
    private final static QName _SOAPWorkflowProofAddRecipientRequestObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofAddRecipientRequestObjectElement");
    private final static QName _SOAPTagElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPTagElement");
    private final static QName _SOAPWorkflowProofRecipientRequestObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofRecipientRequestObjectElement");
    private final static QName _SOAPCustomFieldOptionObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCustomFieldOptionObjectElement");
    private final static QName _SOAPWorkflowTemplateBasicObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowTemplateBasicObjectArrayElement");
    private final static QName _SOAPWorkflowProofRecipientObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofRecipientObjectElement");
    private final static QName _SOAPWorkflowRecipientRequestObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowRecipientRequestObjectArrayElement");
    private final static QName _SOAPCallbackObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCallbackObjectArrayElement");
    private final static QName _SOAPFileEmbedCodeObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileEmbedCodeObjectArrayElement");
    private final static QName _SOAPUserProofingDefaultsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPUserProofingDefaultsObjectElement");
    private final static QName _SOAPUserEmailObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPUserEmailObjectArrayElement");
    private final static QName _SOAPActionObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPActionObjectArrayElement");
    private final static QName _SOAPUserEmailObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPUserEmailObjectElement");
    private final static QName _SOAPWorkflowRecipientObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowRecipientObjectArrayElement");
    private final static QName _SOAPContactObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactObjectElement");
    private final static QName _SOAPFileUploadObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileUploadObjectArrayElement");
    private final static QName _SOAPWorkflowProofAddRecipientRequestObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofAddRecipientRequestObjectArrayElement");
    private final static QName _SOAPWorkflowRecipientRequestUpdateObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowRecipientRequestUpdateObjectElement");
    private final static QName _SOAPCommentReplyObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCommentReplyObjectElement");
    private final static QName _SOAPWorkflowProofStageRequestObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPWorkflowProofStageRequestObjectArrayElement");
    private final static QName _StringArrayElement_QNAME = new QName("https://soap.proofhq.com/", "StringArrayElement");
    private final static QName _SOAPCallbackObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPCallbackObjectElement");
    private final static QName _SOAPDecisionObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPDecisionObjectElement");
    private final static QName _SOAPContactAccountObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPContactAccountObjectArrayElement");
    private final static QName _SOAPFileActivityObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPFileActivityObjectElement");
    private final static QName _SOAPProfileObjectArrayElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPProfileObjectArrayElement");
    private final static QName _SOAPAccountObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPAccountObjectElement");
    private final static QName _SOAPAccountProofingDefaultsObjectElement_QNAME = new QName("https://soap.proofhq.com/", "SOAPAccountProofingDefaultsObjectElement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.soap_proofhq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SOAPDecisionReasonObjectArray }
     * 
     */
    public SOAPDecisionReasonObjectArray createSOAPDecisionReasonObjectArray() {
        return new SOAPDecisionReasonObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofStageObject }
     * 
     */
    public SOAPWorkflowProofStageObject createSOAPWorkflowProofStageObject() {
        return new SOAPWorkflowProofStageObject();
    }

    /**
     * Create an instance of {@link SOAPContactAccountObject }
     * 
     */
    public SOAPContactAccountObject createSOAPContactAccountObject() {
        return new SOAPContactAccountObject();
    }

    /**
     * Create an instance of {@link SOAPFileEmbedCodeObject }
     * 
     */
    public SOAPFileEmbedCodeObject createSOAPFileEmbedCodeObject() {
        return new SOAPFileEmbedCodeObject();
    }

    /**
     * Create an instance of {@link SOAPWorkspaceObjectArray }
     * 
     */
    public SOAPWorkspaceObjectArray createSOAPWorkspaceObjectArray() {
        return new SOAPWorkspaceObjectArray();
    }

    /**
     * Create an instance of {@link AnyTypeArray }
     * 
     */
    public AnyTypeArray createAnyTypeArray() {
        return new AnyTypeArray();
    }

    /**
     * Create an instance of {@link SOAPContactProofingDefaultsObject }
     * 
     */
    public SOAPContactProofingDefaultsObject createSOAPContactProofingDefaultsObject() {
        return new SOAPContactProofingDefaultsObject();
    }

    /**
     * Create an instance of {@link SOAPContactObjectArray }
     * 
     */
    public SOAPContactObjectArray createSOAPContactObjectArray() {
        return new SOAPContactObjectArray();
    }

    /**
     * Create an instance of {@link SOAPCommentObjectArray }
     * 
     */
    public SOAPCommentObjectArray createSOAPCommentObjectArray() {
        return new SOAPCommentObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkspaceObject }
     * 
     */
    public SOAPWorkspaceObject createSOAPWorkspaceObject() {
        return new SOAPWorkspaceObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowStageObject }
     * 
     */
    public SOAPWorkflowStageObject createSOAPWorkflowStageObject() {
        return new SOAPWorkflowStageObject();
    }

    /**
     * Create an instance of {@link SOAPDecisionObjectArray }
     * 
     */
    public SOAPDecisionObjectArray createSOAPDecisionObjectArray() {
        return new SOAPDecisionObjectArray();
    }

    /**
     * Create an instance of {@link SOAPGroupMemberObjectArray }
     * 
     */
    public SOAPGroupMemberObjectArray createSOAPGroupMemberObjectArray() {
        return new SOAPGroupMemberObjectArray();
    }

    /**
     * Create an instance of {@link SOAPFileRecipientObject }
     * 
     */
    public SOAPFileRecipientObject createSOAPFileRecipientObject() {
        return new SOAPFileRecipientObject();
    }

    /**
     * Create an instance of {@link SOAPFileObjectArray }
     * 
     */
    public SOAPFileObjectArray createSOAPFileObjectArray() {
        return new SOAPFileObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowRecipientObject }
     * 
     */
    public SOAPWorkflowRecipientObject createSOAPWorkflowRecipientObject() {
        return new SOAPWorkflowRecipientObject();
    }

    /**
     * Create an instance of {@link SOAPMembershipObject }
     * 
     */
    public SOAPMembershipObject createSOAPMembershipObject() {
        return new SOAPMembershipObject();
    }

    /**
     * Create an instance of {@link SOAPFileObject }
     * 
     */
    public SOAPFileObject createSOAPFileObject() {
        return new SOAPFileObject();
    }

    /**
     * Create an instance of {@link SOAPProofStageObject }
     * 
     */
    public SOAPProofStageObject createSOAPProofStageObject() {
        return new SOAPProofStageObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowStageRequestUpdateObject }
     * 
     */
    public SOAPWorkflowStageRequestUpdateObject createSOAPWorkflowStageRequestUpdateObject() {
        return new SOAPWorkflowStageRequestUpdateObject();
    }

    /**
     * Create an instance of {@link SOAPGroupObject }
     * 
     */
    public SOAPGroupObject createSOAPGroupObject() {
        return new SOAPGroupObject();
    }

    /**
     * Create an instance of {@link SOAPContactGroupObjectArray }
     * 
     */
    public SOAPContactGroupObjectArray createSOAPContactGroupObjectArray() {
        return new SOAPContactGroupObjectArray();
    }

    /**
     * Create an instance of {@link SOAPProfileObject }
     * 
     */
    public SOAPProfileObject createSOAPProfileObject() {
        return new SOAPProfileObject();
    }

    /**
     * Create an instance of {@link SOAPLoginObject }
     * 
     */
    public SOAPLoginObject createSOAPLoginObject() {
        return new SOAPLoginObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowTemplateObject }
     * 
     */
    public SOAPWorkflowTemplateObject createSOAPWorkflowTemplateObject() {
        return new SOAPWorkflowTemplateObject();
    }

    /**
     * Create an instance of {@link SOAPContactGroupObject }
     * 
     */
    public SOAPContactGroupObject createSOAPContactGroupObject() {
        return new SOAPContactGroupObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowStageRequestObjectArray }
     * 
     */
    public SOAPWorkflowStageRequestObjectArray createSOAPWorkflowStageRequestObjectArray() {
        return new SOAPWorkflowStageRequestObjectArray();
    }

    /**
     * Create an instance of {@link SOAPUserObject }
     * 
     */
    public SOAPUserObject createSOAPUserObject() {
        return new SOAPUserObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofAddRecipientRequestObject }
     * 
     */
    public SOAPWorkflowProofAddRecipientRequestObject createSOAPWorkflowProofAddRecipientRequestObject() {
        return new SOAPWorkflowProofAddRecipientRequestObject();
    }

    /**
     * Create an instance of {@link SOAPTag }
     * 
     */
    public SOAPTag createSOAPTag() {
        return new SOAPTag();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofRecipientRequestObject }
     * 
     */
    public SOAPWorkflowProofRecipientRequestObject createSOAPWorkflowProofRecipientRequestObject() {
        return new SOAPWorkflowProofRecipientRequestObject();
    }

    /**
     * Create an instance of {@link SOAPCustomFieldOptionObject }
     * 
     */
    public SOAPCustomFieldOptionObject createSOAPCustomFieldOptionObject() {
        return new SOAPCustomFieldOptionObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowTemplateBasicObjectArray }
     * 
     */
    public SOAPWorkflowTemplateBasicObjectArray createSOAPWorkflowTemplateBasicObjectArray() {
        return new SOAPWorkflowTemplateBasicObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofRecipientObject }
     * 
     */
    public SOAPWorkflowProofRecipientObject createSOAPWorkflowProofRecipientObject() {
        return new SOAPWorkflowProofRecipientObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowRecipientRequestObjectArray }
     * 
     */
    public SOAPWorkflowRecipientRequestObjectArray createSOAPWorkflowRecipientRequestObjectArray() {
        return new SOAPWorkflowRecipientRequestObjectArray();
    }

    /**
     * Create an instance of {@link SOAPCallbackObjectArray }
     * 
     */
    public SOAPCallbackObjectArray createSOAPCallbackObjectArray() {
        return new SOAPCallbackObjectArray();
    }

    /**
     * Create an instance of {@link SOAPFileEmbedCodeObjectArray }
     * 
     */
    public SOAPFileEmbedCodeObjectArray createSOAPFileEmbedCodeObjectArray() {
        return new SOAPFileEmbedCodeObjectArray();
    }

    /**
     * Create an instance of {@link SOAPUserProofingDefaultsObject }
     * 
     */
    public SOAPUserProofingDefaultsObject createSOAPUserProofingDefaultsObject() {
        return new SOAPUserProofingDefaultsObject();
    }

    /**
     * Create an instance of {@link SOAPUserEmailObjectArray }
     * 
     */
    public SOAPUserEmailObjectArray createSOAPUserEmailObjectArray() {
        return new SOAPUserEmailObjectArray();
    }

    /**
     * Create an instance of {@link SOAPActionObjectArray }
     * 
     */
    public SOAPActionObjectArray createSOAPActionObjectArray() {
        return new SOAPActionObjectArray();
    }

    /**
     * Create an instance of {@link SOAPUserEmailObject }
     * 
     */
    public SOAPUserEmailObject createSOAPUserEmailObject() {
        return new SOAPUserEmailObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowRecipientObjectArray }
     * 
     */
    public SOAPWorkflowRecipientObjectArray createSOAPWorkflowRecipientObjectArray() {
        return new SOAPWorkflowRecipientObjectArray();
    }

    /**
     * Create an instance of {@link SOAPContactObject }
     * 
     */
    public SOAPContactObject createSOAPContactObject() {
        return new SOAPContactObject();
    }

    /**
     * Create an instance of {@link SOAPFileUploadObjectArray }
     * 
     */
    public SOAPFileUploadObjectArray createSOAPFileUploadObjectArray() {
        return new SOAPFileUploadObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofAddRecipientRequestObjectArray }
     * 
     */
    public SOAPWorkflowProofAddRecipientRequestObjectArray createSOAPWorkflowProofAddRecipientRequestObjectArray() {
        return new SOAPWorkflowProofAddRecipientRequestObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowRecipientRequestUpdateObject }
     * 
     */
    public SOAPWorkflowRecipientRequestUpdateObject createSOAPWorkflowRecipientRequestUpdateObject() {
        return new SOAPWorkflowRecipientRequestUpdateObject();
    }

    /**
     * Create an instance of {@link SOAPCommentReplyObject }
     * 
     */
    public SOAPCommentReplyObject createSOAPCommentReplyObject() {
        return new SOAPCommentReplyObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofStageRequestObjectArray }
     * 
     */
    public SOAPWorkflowProofStageRequestObjectArray createSOAPWorkflowProofStageRequestObjectArray() {
        return new SOAPWorkflowProofStageRequestObjectArray();
    }

    /**
     * Create an instance of {@link StringArray }
     * 
     */
    public StringArray createStringArray() {
        return new StringArray();
    }

    /**
     * Create an instance of {@link SOAPCallbackObject }
     * 
     */
    public SOAPCallbackObject createSOAPCallbackObject() {
        return new SOAPCallbackObject();
    }

    /**
     * Create an instance of {@link SOAPDecisionObject }
     * 
     */
    public SOAPDecisionObject createSOAPDecisionObject() {
        return new SOAPDecisionObject();
    }

    /**
     * Create an instance of {@link SOAPContactAccountObjectArray }
     * 
     */
    public SOAPContactAccountObjectArray createSOAPContactAccountObjectArray() {
        return new SOAPContactAccountObjectArray();
    }

    /**
     * Create an instance of {@link SOAPFileActivityObject }
     * 
     */
    public SOAPFileActivityObject createSOAPFileActivityObject() {
        return new SOAPFileActivityObject();
    }

    /**
     * Create an instance of {@link SOAPProfileObjectArray }
     * 
     */
    public SOAPProfileObjectArray createSOAPProfileObjectArray() {
        return new SOAPProfileObjectArray();
    }

    /**
     * Create an instance of {@link SOAPAccountObject }
     * 
     */
    public SOAPAccountObject createSOAPAccountObject() {
        return new SOAPAccountObject();
    }

    /**
     * Create an instance of {@link SOAPAccountProofingDefaultsObject }
     * 
     */
    public SOAPAccountProofingDefaultsObject createSOAPAccountProofingDefaultsObject() {
        return new SOAPAccountProofingDefaultsObject();
    }

    /**
     * Create an instance of {@link SOAPUserObjectArray }
     * 
     */
    public SOAPUserObjectArray createSOAPUserObjectArray() {
        return new SOAPUserObjectArray();
    }

    /**
     * Create an instance of {@link IntArray }
     * 
     */
    public IntArray createIntArray() {
        return new IntArray();
    }

    /**
     * Create an instance of {@link SOAPFileListViewObjectArray }
     * 
     */
    public SOAPFileListViewObjectArray createSOAPFileListViewObjectArray() {
        return new SOAPFileListViewObjectArray();
    }

    /**
     * Create an instance of {@link SOAPCommentObject }
     * 
     */
    public SOAPCommentObject createSOAPCommentObject() {
        return new SOAPCommentObject();
    }

    /**
     * Create an instance of {@link SOAPPermissionsObject }
     * 
     */
    public SOAPPermissionsObject createSOAPPermissionsObject() {
        return new SOAPPermissionsObject();
    }

    /**
     * Create an instance of {@link SOAPFileListViewObject }
     * 
     */
    public SOAPFileListViewObject createSOAPFileListViewObject() {
        return new SOAPFileListViewObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofRecipientRequestUpdateObject }
     * 
     */
    public SOAPWorkflowProofRecipientRequestUpdateObject createSOAPWorkflowProofRecipientRequestUpdateObject() {
        return new SOAPWorkflowProofRecipientRequestUpdateObject();
    }

    /**
     * Create an instance of {@link SOAPFileLinkObjectArray }
     * 
     */
    public SOAPFileLinkObjectArray createSOAPFileLinkObjectArray() {
        return new SOAPFileLinkObjectArray();
    }

    /**
     * Create an instance of {@link SOAPFileRecipientObjectArray }
     * 
     */
    public SOAPFileRecipientObjectArray createSOAPFileRecipientObjectArray() {
        return new SOAPFileRecipientObjectArray();
    }

    /**
     * Create an instance of {@link SOAPGroupObjectArray }
     * 
     */
    public SOAPGroupObjectArray createSOAPGroupObjectArray() {
        return new SOAPGroupObjectArray();
    }

    /**
     * Create an instance of {@link SOAPProofStageObjectArray }
     * 
     */
    public SOAPProofStageObjectArray createSOAPProofStageObjectArray() {
        return new SOAPProofStageObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofStageRequestObject }
     * 
     */
    public SOAPWorkflowProofStageRequestObject createSOAPWorkflowProofStageRequestObject() {
        return new SOAPWorkflowProofStageRequestObject();
    }

    /**
     * Create an instance of {@link SOAPImageObject }
     * 
     */
    public SOAPImageObject createSOAPImageObject() {
        return new SOAPImageObject();
    }

    /**
     * Create an instance of {@link SOAPCustomFieldObject }
     * 
     */
    public SOAPCustomFieldObject createSOAPCustomFieldObject() {
        return new SOAPCustomFieldObject();
    }

    /**
     * Create an instance of {@link SOAPSectionObject }
     * 
     */
    public SOAPSectionObject createSOAPSectionObject() {
        return new SOAPSectionObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowStageRequestObject }
     * 
     */
    public SOAPWorkflowStageRequestObject createSOAPWorkflowStageRequestObject() {
        return new SOAPWorkflowStageRequestObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofStageObjectArray }
     * 
     */
    public SOAPWorkflowProofStageObjectArray createSOAPWorkflowProofStageObjectArray() {
        return new SOAPWorkflowProofStageObjectArray();
    }

    /**
     * Create an instance of {@link SOAPSatelliteAccountObject }
     * 
     */
    public SOAPSatelliteAccountObject createSOAPSatelliteAccountObject() {
        return new SOAPSatelliteAccountObject();
    }

    /**
     * Create an instance of {@link SOAPGroupMemberObject }
     * 
     */
    public SOAPGroupMemberObject createSOAPGroupMemberObject() {
        return new SOAPGroupMemberObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofRecipientObjectArray }
     * 
     */
    public SOAPWorkflowProofRecipientObjectArray createSOAPWorkflowProofRecipientObjectArray() {
        return new SOAPWorkflowProofRecipientObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowStageObjectArray }
     * 
     */
    public SOAPWorkflowStageObjectArray createSOAPWorkflowStageObjectArray() {
        return new SOAPWorkflowStageObjectArray();
    }

    /**
     * Create an instance of {@link SOAPAccountPasswordSettingsObject }
     * 
     */
    public SOAPAccountPasswordSettingsObject createSOAPAccountPasswordSettingsObject() {
        return new SOAPAccountPasswordSettingsObject();
    }

    /**
     * Create an instance of {@link SOAPFileUploadObject }
     * 
     */
    public SOAPFileUploadObject createSOAPFileUploadObject() {
        return new SOAPFileUploadObject();
    }

    /**
     * Create an instance of {@link SOAPFileListViewCollectionObject }
     * 
     */
    public SOAPFileListViewCollectionObject createSOAPFileListViewCollectionObject() {
        return new SOAPFileListViewCollectionObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowTemplateBasicObject }
     * 
     */
    public SOAPWorkflowTemplateBasicObject createSOAPWorkflowTemplateBasicObject() {
        return new SOAPWorkflowTemplateBasicObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofObject }
     * 
     */
    public SOAPWorkflowProofObject createSOAPWorkflowProofObject() {
        return new SOAPWorkflowProofObject();
    }

    /**
     * Create an instance of {@link SOAPFileActivityObjectArray }
     * 
     */
    public SOAPFileActivityObjectArray createSOAPFileActivityObjectArray() {
        return new SOAPFileActivityObjectArray();
    }

    /**
     * Create an instance of {@link SOAPCommentReplyObjectArray }
     * 
     */
    public SOAPCommentReplyObjectArray createSOAPCommentReplyObjectArray() {
        return new SOAPCommentReplyObjectArray();
    }

    /**
     * Create an instance of {@link SOAPTagArray }
     * 
     */
    public SOAPTagArray createSOAPTagArray() {
        return new SOAPTagArray();
    }

    /**
     * Create an instance of {@link SOAPDecisionReasonObject }
     * 
     */
    public SOAPDecisionReasonObject createSOAPDecisionReasonObject() {
        return new SOAPDecisionReasonObject();
    }

    /**
     * Create an instance of {@link SOAPWorkflowRecipientRequestObject }
     * 
     */
    public SOAPWorkflowRecipientRequestObject createSOAPWorkflowRecipientRequestObject() {
        return new SOAPWorkflowRecipientRequestObject();
    }

    /**
     * Create an instance of {@link SOAPCustomFieldObjectArray }
     * 
     */
    public SOAPCustomFieldObjectArray createSOAPCustomFieldObjectArray() {
        return new SOAPCustomFieldObjectArray();
    }

    /**
     * Create an instance of {@link SOAPProgressObject }
     * 
     */
    public SOAPProgressObject createSOAPProgressObject() {
        return new SOAPProgressObject();
    }

    /**
     * Create an instance of {@link SOAPPlanObjectArray }
     * 
     */
    public SOAPPlanObjectArray createSOAPPlanObjectArray() {
        return new SOAPPlanObjectArray();
    }

    /**
     * Create an instance of {@link SOAPRecipientDecisionReasonObject }
     * 
     */
    public SOAPRecipientDecisionReasonObject createSOAPRecipientDecisionReasonObject() {
        return new SOAPRecipientDecisionReasonObject();
    }

    /**
     * Create an instance of {@link SOAPCustomFieldOptionObjectArray }
     * 
     */
    public SOAPCustomFieldOptionObjectArray createSOAPCustomFieldOptionObjectArray() {
        return new SOAPCustomFieldOptionObjectArray();
    }

    /**
     * Create an instance of {@link SOAPFileSettingsObject }
     * 
     */
    public SOAPFileSettingsObject createSOAPFileSettingsObject() {
        return new SOAPFileSettingsObject();
    }

    /**
     * Create an instance of {@link SOAPFileLinkObject }
     * 
     */
    public SOAPFileLinkObject createSOAPFileLinkObject() {
        return new SOAPFileLinkObject();
    }

    /**
     * Create an instance of {@link SOAPPlanObject }
     * 
     */
    public SOAPPlanObject createSOAPPlanObject() {
        return new SOAPPlanObject();
    }

    /**
     * Create an instance of {@link SOAPSectionObjectArray }
     * 
     */
    public SOAPSectionObjectArray createSOAPSectionObjectArray() {
        return new SOAPSectionObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofObjectArray }
     * 
     */
    public SOAPWorkflowProofObjectArray createSOAPWorkflowProofObjectArray() {
        return new SOAPWorkflowProofObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofStageRequestUpdateObject }
     * 
     */
    public SOAPWorkflowProofStageRequestUpdateObject createSOAPWorkflowProofStageRequestUpdateObject() {
        return new SOAPWorkflowProofStageRequestUpdateObject();
    }

    /**
     * Create an instance of {@link SOAPSatelliteAccountObjectArray }
     * 
     */
    public SOAPSatelliteAccountObjectArray createSOAPSatelliteAccountObjectArray() {
        return new SOAPSatelliteAccountObjectArray();
    }

    /**
     * Create an instance of {@link SOAPWorkflowProofRecipientRequestObjectArray }
     * 
     */
    public SOAPWorkflowProofRecipientRequestObjectArray createSOAPWorkflowProofRecipientRequestObjectArray() {
        return new SOAPWorkflowProofRecipientRequestObjectArray();
    }

    /**
     * Create an instance of {@link SOAPRecipientDecisionReasonObjectArray }
     * 
     */
    public SOAPRecipientDecisionReasonObjectArray createSOAPRecipientDecisionReasonObjectArray() {
        return new SOAPRecipientDecisionReasonObjectArray();
    }

    /**
     * Create an instance of {@link SOAPRecipientObject }
     * 
     */
    public SOAPRecipientObject createSOAPRecipientObject() {
        return new SOAPRecipientObject();
    }

    /**
     * Create an instance of {@link SOAPRecipientObjectArray }
     * 
     */
    public SOAPRecipientObjectArray createSOAPRecipientObjectArray() {
        return new SOAPRecipientObjectArray();
    }

    /**
     * Create an instance of {@link SOAPActionObject }
     * 
     */
    public SOAPActionObject createSOAPActionObject() {
        return new SOAPActionObject();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPUserObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPUserObjectArrayElement")
    public JAXBElement<SOAPUserObjectArray> createSOAPUserObjectArrayElement(SOAPUserObjectArray value) {
        return new JAXBElement<SOAPUserObjectArray>(_SOAPUserObjectArrayElement_QNAME, SOAPUserObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "IntArrayElement")
    public JAXBElement<IntArray> createIntArrayElement(IntArray value) {
        return new JAXBElement<IntArray>(_IntArrayElement_QNAME, IntArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileListViewObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileListViewObjectArrayElement")
    public JAXBElement<SOAPFileListViewObjectArray> createSOAPFileListViewObjectArrayElement(SOAPFileListViewObjectArray value) {
        return new JAXBElement<SOAPFileListViewObjectArray>(_SOAPFileListViewObjectArrayElement_QNAME, SOAPFileListViewObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCommentObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCommentObjectElement")
    public JAXBElement<SOAPCommentObject> createSOAPCommentObjectElement(SOAPCommentObject value) {
        return new JAXBElement<SOAPCommentObject>(_SOAPCommentObjectElement_QNAME, SOAPCommentObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPPermissionsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPPermissionsObjectElement")
    public JAXBElement<SOAPPermissionsObject> createSOAPPermissionsObjectElement(SOAPPermissionsObject value) {
        return new JAXBElement<SOAPPermissionsObject>(_SOAPPermissionsObjectElement_QNAME, SOAPPermissionsObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileListViewObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileListViewObjectElement")
    public JAXBElement<SOAPFileListViewObject> createSOAPFileListViewObjectElement(SOAPFileListViewObject value) {
        return new JAXBElement<SOAPFileListViewObject>(_SOAPFileListViewObjectElement_QNAME, SOAPFileListViewObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofRecipientRequestUpdateObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofRecipientRequestUpdateObjectElement")
    public JAXBElement<SOAPWorkflowProofRecipientRequestUpdateObject> createSOAPWorkflowProofRecipientRequestUpdateObjectElement(SOAPWorkflowProofRecipientRequestUpdateObject value) {
        return new JAXBElement<SOAPWorkflowProofRecipientRequestUpdateObject>(_SOAPWorkflowProofRecipientRequestUpdateObjectElement_QNAME, SOAPWorkflowProofRecipientRequestUpdateObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileLinkObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileLinkObjectArrayElement")
    public JAXBElement<SOAPFileLinkObjectArray> createSOAPFileLinkObjectArrayElement(SOAPFileLinkObjectArray value) {
        return new JAXBElement<SOAPFileLinkObjectArray>(_SOAPFileLinkObjectArrayElement_QNAME, SOAPFileLinkObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileRecipientObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileRecipientObjectArrayElement")
    public JAXBElement<SOAPFileRecipientObjectArray> createSOAPFileRecipientObjectArrayElement(SOAPFileRecipientObjectArray value) {
        return new JAXBElement<SOAPFileRecipientObjectArray>(_SOAPFileRecipientObjectArrayElement_QNAME, SOAPFileRecipientObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPGroupObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPGroupObjectArrayElement")
    public JAXBElement<SOAPGroupObjectArray> createSOAPGroupObjectArrayElement(SOAPGroupObjectArray value) {
        return new JAXBElement<SOAPGroupObjectArray>(_SOAPGroupObjectArrayElement_QNAME, SOAPGroupObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPProofStageObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPProofStageObjectArrayElement")
    public JAXBElement<SOAPProofStageObjectArray> createSOAPProofStageObjectArrayElement(SOAPProofStageObjectArray value) {
        return new JAXBElement<SOAPProofStageObjectArray>(_SOAPProofStageObjectArrayElement_QNAME, SOAPProofStageObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofStageRequestObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofStageRequestObjectElement")
    public JAXBElement<SOAPWorkflowProofStageRequestObject> createSOAPWorkflowProofStageRequestObjectElement(SOAPWorkflowProofStageRequestObject value) {
        return new JAXBElement<SOAPWorkflowProofStageRequestObject>(_SOAPWorkflowProofStageRequestObjectElement_QNAME, SOAPWorkflowProofStageRequestObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPImageObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPImageObjectElement")
    public JAXBElement<SOAPImageObject> createSOAPImageObjectElement(SOAPImageObject value) {
        return new JAXBElement<SOAPImageObject>(_SOAPImageObjectElement_QNAME, SOAPImageObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCustomFieldObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCustomFieldObjectElement")
    public JAXBElement<SOAPCustomFieldObject> createSOAPCustomFieldObjectElement(SOAPCustomFieldObject value) {
        return new JAXBElement<SOAPCustomFieldObject>(_SOAPCustomFieldObjectElement_QNAME, SOAPCustomFieldObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPSectionObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPSectionObjectElement")
    public JAXBElement<SOAPSectionObject> createSOAPSectionObjectElement(SOAPSectionObject value) {
        return new JAXBElement<SOAPSectionObject>(_SOAPSectionObjectElement_QNAME, SOAPSectionObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowStageRequestObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowStageRequestObjectElement")
    public JAXBElement<SOAPWorkflowStageRequestObject> createSOAPWorkflowStageRequestObjectElement(SOAPWorkflowStageRequestObject value) {
        return new JAXBElement<SOAPWorkflowStageRequestObject>(_SOAPWorkflowStageRequestObjectElement_QNAME, SOAPWorkflowStageRequestObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofStageObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofStageObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofStageObjectArray> createSOAPWorkflowProofStageObjectArrayElement(SOAPWorkflowProofStageObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofStageObjectArray>(_SOAPWorkflowProofStageObjectArrayElement_QNAME, SOAPWorkflowProofStageObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPSatelliteAccountObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPSatelliteAccountObjectElement")
    public JAXBElement<SOAPSatelliteAccountObject> createSOAPSatelliteAccountObjectElement(SOAPSatelliteAccountObject value) {
        return new JAXBElement<SOAPSatelliteAccountObject>(_SOAPSatelliteAccountObjectElement_QNAME, SOAPSatelliteAccountObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPGroupMemberObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPGroupMemberObjectElement")
    public JAXBElement<SOAPGroupMemberObject> createSOAPGroupMemberObjectElement(SOAPGroupMemberObject value) {
        return new JAXBElement<SOAPGroupMemberObject>(_SOAPGroupMemberObjectElement_QNAME, SOAPGroupMemberObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofRecipientObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofRecipientObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofRecipientObjectArray> createSOAPWorkflowProofRecipientObjectArrayElement(SOAPWorkflowProofRecipientObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofRecipientObjectArray>(_SOAPWorkflowProofRecipientObjectArrayElement_QNAME, SOAPWorkflowProofRecipientObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowStageObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowStageObjectArrayElement")
    public JAXBElement<SOAPWorkflowStageObjectArray> createSOAPWorkflowStageObjectArrayElement(SOAPWorkflowStageObjectArray value) {
        return new JAXBElement<SOAPWorkflowStageObjectArray>(_SOAPWorkflowStageObjectArrayElement_QNAME, SOAPWorkflowStageObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPAccountPasswordSettingsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPAccountPasswordSettingsObjectElement")
    public JAXBElement<SOAPAccountPasswordSettingsObject> createSOAPAccountPasswordSettingsObjectElement(SOAPAccountPasswordSettingsObject value) {
        return new JAXBElement<SOAPAccountPasswordSettingsObject>(_SOAPAccountPasswordSettingsObjectElement_QNAME, SOAPAccountPasswordSettingsObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileUploadObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileUploadObjectElement")
    public JAXBElement<SOAPFileUploadObject> createSOAPFileUploadObjectElement(SOAPFileUploadObject value) {
        return new JAXBElement<SOAPFileUploadObject>(_SOAPFileUploadObjectElement_QNAME, SOAPFileUploadObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileListViewCollectionObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileListViewCollectionObjectElement")
    public JAXBElement<SOAPFileListViewCollectionObject> createSOAPFileListViewCollectionObjectElement(SOAPFileListViewCollectionObject value) {
        return new JAXBElement<SOAPFileListViewCollectionObject>(_SOAPFileListViewCollectionObjectElement_QNAME, SOAPFileListViewCollectionObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowTemplateBasicObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowTemplateBasicObjectElement")
    public JAXBElement<SOAPWorkflowTemplateBasicObject> createSOAPWorkflowTemplateBasicObjectElement(SOAPWorkflowTemplateBasicObject value) {
        return new JAXBElement<SOAPWorkflowTemplateBasicObject>(_SOAPWorkflowTemplateBasicObjectElement_QNAME, SOAPWorkflowTemplateBasicObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofObjectElement")
    public JAXBElement<SOAPWorkflowProofObject> createSOAPWorkflowProofObjectElement(SOAPWorkflowProofObject value) {
        return new JAXBElement<SOAPWorkflowProofObject>(_SOAPWorkflowProofObjectElement_QNAME, SOAPWorkflowProofObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileActivityObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileActivityObjectArrayElement")
    public JAXBElement<SOAPFileActivityObjectArray> createSOAPFileActivityObjectArrayElement(SOAPFileActivityObjectArray value) {
        return new JAXBElement<SOAPFileActivityObjectArray>(_SOAPFileActivityObjectArrayElement_QNAME, SOAPFileActivityObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCommentReplyObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCommentReplyObjectArrayElement")
    public JAXBElement<SOAPCommentReplyObjectArray> createSOAPCommentReplyObjectArrayElement(SOAPCommentReplyObjectArray value) {
        return new JAXBElement<SOAPCommentReplyObjectArray>(_SOAPCommentReplyObjectArrayElement_QNAME, SOAPCommentReplyObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPTagArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPTagArrayElement")
    public JAXBElement<SOAPTagArray> createSOAPTagArrayElement(SOAPTagArray value) {
        return new JAXBElement<SOAPTagArray>(_SOAPTagArrayElement_QNAME, SOAPTagArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPDecisionReasonObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPDecisionReasonObjectElement")
    public JAXBElement<SOAPDecisionReasonObject> createSOAPDecisionReasonObjectElement(SOAPDecisionReasonObject value) {
        return new JAXBElement<SOAPDecisionReasonObject>(_SOAPDecisionReasonObjectElement_QNAME, SOAPDecisionReasonObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowRecipientRequestObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowRecipientRequestObjectElement")
    public JAXBElement<SOAPWorkflowRecipientRequestObject> createSOAPWorkflowRecipientRequestObjectElement(SOAPWorkflowRecipientRequestObject value) {
        return new JAXBElement<SOAPWorkflowRecipientRequestObject>(_SOAPWorkflowRecipientRequestObjectElement_QNAME, SOAPWorkflowRecipientRequestObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCustomFieldObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCustomFieldObjectArrayElement")
    public JAXBElement<SOAPCustomFieldObjectArray> createSOAPCustomFieldObjectArrayElement(SOAPCustomFieldObjectArray value) {
        return new JAXBElement<SOAPCustomFieldObjectArray>(_SOAPCustomFieldObjectArrayElement_QNAME, SOAPCustomFieldObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPProgressObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPProgressObjectElement")
    public JAXBElement<SOAPProgressObject> createSOAPProgressObjectElement(SOAPProgressObject value) {
        return new JAXBElement<SOAPProgressObject>(_SOAPProgressObjectElement_QNAME, SOAPProgressObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPPlanObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPPlanObjectArrayElement")
    public JAXBElement<SOAPPlanObjectArray> createSOAPPlanObjectArrayElement(SOAPPlanObjectArray value) {
        return new JAXBElement<SOAPPlanObjectArray>(_SOAPPlanObjectArrayElement_QNAME, SOAPPlanObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPRecipientDecisionReasonObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPRecipientDecisionReasonObjectElement")
    public JAXBElement<SOAPRecipientDecisionReasonObject> createSOAPRecipientDecisionReasonObjectElement(SOAPRecipientDecisionReasonObject value) {
        return new JAXBElement<SOAPRecipientDecisionReasonObject>(_SOAPRecipientDecisionReasonObjectElement_QNAME, SOAPRecipientDecisionReasonObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCustomFieldOptionObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCustomFieldOptionObjectArrayElement")
    public JAXBElement<SOAPCustomFieldOptionObjectArray> createSOAPCustomFieldOptionObjectArrayElement(SOAPCustomFieldOptionObjectArray value) {
        return new JAXBElement<SOAPCustomFieldOptionObjectArray>(_SOAPCustomFieldOptionObjectArrayElement_QNAME, SOAPCustomFieldOptionObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileSettingsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileSettingsObjectElement")
    public JAXBElement<SOAPFileSettingsObject> createSOAPFileSettingsObjectElement(SOAPFileSettingsObject value) {
        return new JAXBElement<SOAPFileSettingsObject>(_SOAPFileSettingsObjectElement_QNAME, SOAPFileSettingsObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileLinkObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileLinkObjectElement")
    public JAXBElement<SOAPFileLinkObject> createSOAPFileLinkObjectElement(SOAPFileLinkObject value) {
        return new JAXBElement<SOAPFileLinkObject>(_SOAPFileLinkObjectElement_QNAME, SOAPFileLinkObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPPlanObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPPlanObjectElement")
    public JAXBElement<SOAPPlanObject> createSOAPPlanObjectElement(SOAPPlanObject value) {
        return new JAXBElement<SOAPPlanObject>(_SOAPPlanObjectElement_QNAME, SOAPPlanObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPSectionObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPSectionObjectArrayElement")
    public JAXBElement<SOAPSectionObjectArray> createSOAPSectionObjectArrayElement(SOAPSectionObjectArray value) {
        return new JAXBElement<SOAPSectionObjectArray>(_SOAPSectionObjectArrayElement_QNAME, SOAPSectionObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofObjectArray> createSOAPWorkflowProofObjectArrayElement(SOAPWorkflowProofObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofObjectArray>(_SOAPWorkflowProofObjectArrayElement_QNAME, SOAPWorkflowProofObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofStageRequestUpdateObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofStageRequestUpdateObjectElement")
    public JAXBElement<SOAPWorkflowProofStageRequestUpdateObject> createSOAPWorkflowProofStageRequestUpdateObjectElement(SOAPWorkflowProofStageRequestUpdateObject value) {
        return new JAXBElement<SOAPWorkflowProofStageRequestUpdateObject>(_SOAPWorkflowProofStageRequestUpdateObjectElement_QNAME, SOAPWorkflowProofStageRequestUpdateObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPSatelliteAccountObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPSatelliteAccountObjectArrayElement")
    public JAXBElement<SOAPSatelliteAccountObjectArray> createSOAPSatelliteAccountObjectArrayElement(SOAPSatelliteAccountObjectArray value) {
        return new JAXBElement<SOAPSatelliteAccountObjectArray>(_SOAPSatelliteAccountObjectArrayElement_QNAME, SOAPSatelliteAccountObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofRecipientRequestObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofRecipientRequestObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofRecipientRequestObjectArray> createSOAPWorkflowProofRecipientRequestObjectArrayElement(SOAPWorkflowProofRecipientRequestObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofRecipientRequestObjectArray>(_SOAPWorkflowProofRecipientRequestObjectArrayElement_QNAME, SOAPWorkflowProofRecipientRequestObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPRecipientDecisionReasonObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPRecipientDecisionReasonObjectArrayElement")
    public JAXBElement<SOAPRecipientDecisionReasonObjectArray> createSOAPRecipientDecisionReasonObjectArrayElement(SOAPRecipientDecisionReasonObjectArray value) {
        return new JAXBElement<SOAPRecipientDecisionReasonObjectArray>(_SOAPRecipientDecisionReasonObjectArrayElement_QNAME, SOAPRecipientDecisionReasonObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPRecipientObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPRecipientObjectElement")
    public JAXBElement<SOAPRecipientObject> createSOAPRecipientObjectElement(SOAPRecipientObject value) {
        return new JAXBElement<SOAPRecipientObject>(_SOAPRecipientObjectElement_QNAME, SOAPRecipientObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPRecipientObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPRecipientObjectArrayElement")
    public JAXBElement<SOAPRecipientObjectArray> createSOAPRecipientObjectArrayElement(SOAPRecipientObjectArray value) {
        return new JAXBElement<SOAPRecipientObjectArray>(_SOAPRecipientObjectArrayElement_QNAME, SOAPRecipientObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPActionObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPActionObjectElement")
    public JAXBElement<SOAPActionObject> createSOAPActionObjectElement(SOAPActionObject value) {
        return new JAXBElement<SOAPActionObject>(_SOAPActionObjectElement_QNAME, SOAPActionObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPDecisionReasonObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPDecisionReasonObjectArrayElement")
    public JAXBElement<SOAPDecisionReasonObjectArray> createSOAPDecisionReasonObjectArrayElement(SOAPDecisionReasonObjectArray value) {
        return new JAXBElement<SOAPDecisionReasonObjectArray>(_SOAPDecisionReasonObjectArrayElement_QNAME, SOAPDecisionReasonObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofStageObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofStageObjectElement")
    public JAXBElement<SOAPWorkflowProofStageObject> createSOAPWorkflowProofStageObjectElement(SOAPWorkflowProofStageObject value) {
        return new JAXBElement<SOAPWorkflowProofStageObject>(_SOAPWorkflowProofStageObjectElement_QNAME, SOAPWorkflowProofStageObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactAccountObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactAccountObjectElement")
    public JAXBElement<SOAPContactAccountObject> createSOAPContactAccountObjectElement(SOAPContactAccountObject value) {
        return new JAXBElement<SOAPContactAccountObject>(_SOAPContactAccountObjectElement_QNAME, SOAPContactAccountObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileEmbedCodeObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileEmbedCodeObjectElement")
    public JAXBElement<SOAPFileEmbedCodeObject> createSOAPFileEmbedCodeObjectElement(SOAPFileEmbedCodeObject value) {
        return new JAXBElement<SOAPFileEmbedCodeObject>(_SOAPFileEmbedCodeObjectElement_QNAME, SOAPFileEmbedCodeObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkspaceObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkspaceObjectArrayElement")
    public JAXBElement<SOAPWorkspaceObjectArray> createSOAPWorkspaceObjectArrayElement(SOAPWorkspaceObjectArray value) {
        return new JAXBElement<SOAPWorkspaceObjectArray>(_SOAPWorkspaceObjectArrayElement_QNAME, SOAPWorkspaceObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnyTypeArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "AnyTypeArrayElement")
    public JAXBElement<AnyTypeArray> createAnyTypeArrayElement(AnyTypeArray value) {
        return new JAXBElement<AnyTypeArray>(_AnyTypeArrayElement_QNAME, AnyTypeArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactProofingDefaultsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactProofingDefaultsObjectElement")
    public JAXBElement<SOAPContactProofingDefaultsObject> createSOAPContactProofingDefaultsObjectElement(SOAPContactProofingDefaultsObject value) {
        return new JAXBElement<SOAPContactProofingDefaultsObject>(_SOAPContactProofingDefaultsObjectElement_QNAME, SOAPContactProofingDefaultsObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactObjectArrayElement")
    public JAXBElement<SOAPContactObjectArray> createSOAPContactObjectArrayElement(SOAPContactObjectArray value) {
        return new JAXBElement<SOAPContactObjectArray>(_SOAPContactObjectArrayElement_QNAME, SOAPContactObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCommentObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCommentObjectArrayElement")
    public JAXBElement<SOAPCommentObjectArray> createSOAPCommentObjectArrayElement(SOAPCommentObjectArray value) {
        return new JAXBElement<SOAPCommentObjectArray>(_SOAPCommentObjectArrayElement_QNAME, SOAPCommentObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkspaceObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkspaceObjectElement")
    public JAXBElement<SOAPWorkspaceObject> createSOAPWorkspaceObjectElement(SOAPWorkspaceObject value) {
        return new JAXBElement<SOAPWorkspaceObject>(_SOAPWorkspaceObjectElement_QNAME, SOAPWorkspaceObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowStageObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowStageObjectElement")
    public JAXBElement<SOAPWorkflowStageObject> createSOAPWorkflowStageObjectElement(SOAPWorkflowStageObject value) {
        return new JAXBElement<SOAPWorkflowStageObject>(_SOAPWorkflowStageObjectElement_QNAME, SOAPWorkflowStageObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPDecisionObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPDecisionObjectArrayElement")
    public JAXBElement<SOAPDecisionObjectArray> createSOAPDecisionObjectArrayElement(SOAPDecisionObjectArray value) {
        return new JAXBElement<SOAPDecisionObjectArray>(_SOAPDecisionObjectArrayElement_QNAME, SOAPDecisionObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPGroupMemberObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPGroupMemberObjectArrayElement")
    public JAXBElement<SOAPGroupMemberObjectArray> createSOAPGroupMemberObjectArrayElement(SOAPGroupMemberObjectArray value) {
        return new JAXBElement<SOAPGroupMemberObjectArray>(_SOAPGroupMemberObjectArrayElement_QNAME, SOAPGroupMemberObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileRecipientObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileRecipientObjectElement")
    public JAXBElement<SOAPFileRecipientObject> createSOAPFileRecipientObjectElement(SOAPFileRecipientObject value) {
        return new JAXBElement<SOAPFileRecipientObject>(_SOAPFileRecipientObjectElement_QNAME, SOAPFileRecipientObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileObjectArrayElement")
    public JAXBElement<SOAPFileObjectArray> createSOAPFileObjectArrayElement(SOAPFileObjectArray value) {
        return new JAXBElement<SOAPFileObjectArray>(_SOAPFileObjectArrayElement_QNAME, SOAPFileObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowRecipientObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowRecipientObjectElement")
    public JAXBElement<SOAPWorkflowRecipientObject> createSOAPWorkflowRecipientObjectElement(SOAPWorkflowRecipientObject value) {
        return new JAXBElement<SOAPWorkflowRecipientObject>(_SOAPWorkflowRecipientObjectElement_QNAME, SOAPWorkflowRecipientObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPMembershipObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPMembershipObjectElement")
    public JAXBElement<SOAPMembershipObject> createSOAPMembershipObjectElement(SOAPMembershipObject value) {
        return new JAXBElement<SOAPMembershipObject>(_SOAPMembershipObjectElement_QNAME, SOAPMembershipObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileObjectElement")
    public JAXBElement<SOAPFileObject> createSOAPFileObjectElement(SOAPFileObject value) {
        return new JAXBElement<SOAPFileObject>(_SOAPFileObjectElement_QNAME, SOAPFileObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPProofStageObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPProofStageObjectElement")
    public JAXBElement<SOAPProofStageObject> createSOAPProofStageObjectElement(SOAPProofStageObject value) {
        return new JAXBElement<SOAPProofStageObject>(_SOAPProofStageObjectElement_QNAME, SOAPProofStageObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowStageRequestUpdateObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowStageRequestUpdateObjectElement")
    public JAXBElement<SOAPWorkflowStageRequestUpdateObject> createSOAPWorkflowStageRequestUpdateObjectElement(SOAPWorkflowStageRequestUpdateObject value) {
        return new JAXBElement<SOAPWorkflowStageRequestUpdateObject>(_SOAPWorkflowStageRequestUpdateObjectElement_QNAME, SOAPWorkflowStageRequestUpdateObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPGroupObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPGroupObjectElement")
    public JAXBElement<SOAPGroupObject> createSOAPGroupObjectElement(SOAPGroupObject value) {
        return new JAXBElement<SOAPGroupObject>(_SOAPGroupObjectElement_QNAME, SOAPGroupObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactGroupObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactGroupObjectArrayElement")
    public JAXBElement<SOAPContactGroupObjectArray> createSOAPContactGroupObjectArrayElement(SOAPContactGroupObjectArray value) {
        return new JAXBElement<SOAPContactGroupObjectArray>(_SOAPContactGroupObjectArrayElement_QNAME, SOAPContactGroupObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPProfileObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPProfileObjectElement")
    public JAXBElement<SOAPProfileObject> createSOAPProfileObjectElement(SOAPProfileObject value) {
        return new JAXBElement<SOAPProfileObject>(_SOAPProfileObjectElement_QNAME, SOAPProfileObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPLoginObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPLoginObjectElement")
    public JAXBElement<SOAPLoginObject> createSOAPLoginObjectElement(SOAPLoginObject value) {
        return new JAXBElement<SOAPLoginObject>(_SOAPLoginObjectElement_QNAME, SOAPLoginObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowTemplateObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowTemplateObjectElement")
    public JAXBElement<SOAPWorkflowTemplateObject> createSOAPWorkflowTemplateObjectElement(SOAPWorkflowTemplateObject value) {
        return new JAXBElement<SOAPWorkflowTemplateObject>(_SOAPWorkflowTemplateObjectElement_QNAME, SOAPWorkflowTemplateObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactGroupObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactGroupObjectElement")
    public JAXBElement<SOAPContactGroupObject> createSOAPContactGroupObjectElement(SOAPContactGroupObject value) {
        return new JAXBElement<SOAPContactGroupObject>(_SOAPContactGroupObjectElement_QNAME, SOAPContactGroupObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowStageRequestObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowStageRequestObjectArrayElement")
    public JAXBElement<SOAPWorkflowStageRequestObjectArray> createSOAPWorkflowStageRequestObjectArrayElement(SOAPWorkflowStageRequestObjectArray value) {
        return new JAXBElement<SOAPWorkflowStageRequestObjectArray>(_SOAPWorkflowStageRequestObjectArrayElement_QNAME, SOAPWorkflowStageRequestObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPUserObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPUserObjectElement")
    public JAXBElement<SOAPUserObject> createSOAPUserObjectElement(SOAPUserObject value) {
        return new JAXBElement<SOAPUserObject>(_SOAPUserObjectElement_QNAME, SOAPUserObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofAddRecipientRequestObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofAddRecipientRequestObjectElement")
    public JAXBElement<SOAPWorkflowProofAddRecipientRequestObject> createSOAPWorkflowProofAddRecipientRequestObjectElement(SOAPWorkflowProofAddRecipientRequestObject value) {
        return new JAXBElement<SOAPWorkflowProofAddRecipientRequestObject>(_SOAPWorkflowProofAddRecipientRequestObjectElement_QNAME, SOAPWorkflowProofAddRecipientRequestObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPTag }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPTagElement")
    public JAXBElement<SOAPTag> createSOAPTagElement(SOAPTag value) {
        return new JAXBElement<SOAPTag>(_SOAPTagElement_QNAME, SOAPTag.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofRecipientRequestObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofRecipientRequestObjectElement")
    public JAXBElement<SOAPWorkflowProofRecipientRequestObject> createSOAPWorkflowProofRecipientRequestObjectElement(SOAPWorkflowProofRecipientRequestObject value) {
        return new JAXBElement<SOAPWorkflowProofRecipientRequestObject>(_SOAPWorkflowProofRecipientRequestObjectElement_QNAME, SOAPWorkflowProofRecipientRequestObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCustomFieldOptionObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCustomFieldOptionObjectElement")
    public JAXBElement<SOAPCustomFieldOptionObject> createSOAPCustomFieldOptionObjectElement(SOAPCustomFieldOptionObject value) {
        return new JAXBElement<SOAPCustomFieldOptionObject>(_SOAPCustomFieldOptionObjectElement_QNAME, SOAPCustomFieldOptionObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowTemplateBasicObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowTemplateBasicObjectArrayElement")
    public JAXBElement<SOAPWorkflowTemplateBasicObjectArray> createSOAPWorkflowTemplateBasicObjectArrayElement(SOAPWorkflowTemplateBasicObjectArray value) {
        return new JAXBElement<SOAPWorkflowTemplateBasicObjectArray>(_SOAPWorkflowTemplateBasicObjectArrayElement_QNAME, SOAPWorkflowTemplateBasicObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofRecipientObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofRecipientObjectElement")
    public JAXBElement<SOAPWorkflowProofRecipientObject> createSOAPWorkflowProofRecipientObjectElement(SOAPWorkflowProofRecipientObject value) {
        return new JAXBElement<SOAPWorkflowProofRecipientObject>(_SOAPWorkflowProofRecipientObjectElement_QNAME, SOAPWorkflowProofRecipientObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowRecipientRequestObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowRecipientRequestObjectArrayElement")
    public JAXBElement<SOAPWorkflowRecipientRequestObjectArray> createSOAPWorkflowRecipientRequestObjectArrayElement(SOAPWorkflowRecipientRequestObjectArray value) {
        return new JAXBElement<SOAPWorkflowRecipientRequestObjectArray>(_SOAPWorkflowRecipientRequestObjectArrayElement_QNAME, SOAPWorkflowRecipientRequestObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCallbackObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCallbackObjectArrayElement")
    public JAXBElement<SOAPCallbackObjectArray> createSOAPCallbackObjectArrayElement(SOAPCallbackObjectArray value) {
        return new JAXBElement<SOAPCallbackObjectArray>(_SOAPCallbackObjectArrayElement_QNAME, SOAPCallbackObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileEmbedCodeObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileEmbedCodeObjectArrayElement")
    public JAXBElement<SOAPFileEmbedCodeObjectArray> createSOAPFileEmbedCodeObjectArrayElement(SOAPFileEmbedCodeObjectArray value) {
        return new JAXBElement<SOAPFileEmbedCodeObjectArray>(_SOAPFileEmbedCodeObjectArrayElement_QNAME, SOAPFileEmbedCodeObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPUserProofingDefaultsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPUserProofingDefaultsObjectElement")
    public JAXBElement<SOAPUserProofingDefaultsObject> createSOAPUserProofingDefaultsObjectElement(SOAPUserProofingDefaultsObject value) {
        return new JAXBElement<SOAPUserProofingDefaultsObject>(_SOAPUserProofingDefaultsObjectElement_QNAME, SOAPUserProofingDefaultsObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPUserEmailObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPUserEmailObjectArrayElement")
    public JAXBElement<SOAPUserEmailObjectArray> createSOAPUserEmailObjectArrayElement(SOAPUserEmailObjectArray value) {
        return new JAXBElement<SOAPUserEmailObjectArray>(_SOAPUserEmailObjectArrayElement_QNAME, SOAPUserEmailObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPActionObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPActionObjectArrayElement")
    public JAXBElement<SOAPActionObjectArray> createSOAPActionObjectArrayElement(SOAPActionObjectArray value) {
        return new JAXBElement<SOAPActionObjectArray>(_SOAPActionObjectArrayElement_QNAME, SOAPActionObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPUserEmailObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPUserEmailObjectElement")
    public JAXBElement<SOAPUserEmailObject> createSOAPUserEmailObjectElement(SOAPUserEmailObject value) {
        return new JAXBElement<SOAPUserEmailObject>(_SOAPUserEmailObjectElement_QNAME, SOAPUserEmailObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowRecipientObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowRecipientObjectArrayElement")
    public JAXBElement<SOAPWorkflowRecipientObjectArray> createSOAPWorkflowRecipientObjectArrayElement(SOAPWorkflowRecipientObjectArray value) {
        return new JAXBElement<SOAPWorkflowRecipientObjectArray>(_SOAPWorkflowRecipientObjectArrayElement_QNAME, SOAPWorkflowRecipientObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactObjectElement")
    public JAXBElement<SOAPContactObject> createSOAPContactObjectElement(SOAPContactObject value) {
        return new JAXBElement<SOAPContactObject>(_SOAPContactObjectElement_QNAME, SOAPContactObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileUploadObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileUploadObjectArrayElement")
    public JAXBElement<SOAPFileUploadObjectArray> createSOAPFileUploadObjectArrayElement(SOAPFileUploadObjectArray value) {
        return new JAXBElement<SOAPFileUploadObjectArray>(_SOAPFileUploadObjectArrayElement_QNAME, SOAPFileUploadObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofAddRecipientRequestObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofAddRecipientRequestObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofAddRecipientRequestObjectArray> createSOAPWorkflowProofAddRecipientRequestObjectArrayElement(SOAPWorkflowProofAddRecipientRequestObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofAddRecipientRequestObjectArray>(_SOAPWorkflowProofAddRecipientRequestObjectArrayElement_QNAME, SOAPWorkflowProofAddRecipientRequestObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowRecipientRequestUpdateObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowRecipientRequestUpdateObjectElement")
    public JAXBElement<SOAPWorkflowRecipientRequestUpdateObject> createSOAPWorkflowRecipientRequestUpdateObjectElement(SOAPWorkflowRecipientRequestUpdateObject value) {
        return new JAXBElement<SOAPWorkflowRecipientRequestUpdateObject>(_SOAPWorkflowRecipientRequestUpdateObjectElement_QNAME, SOAPWorkflowRecipientRequestUpdateObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCommentReplyObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCommentReplyObjectElement")
    public JAXBElement<SOAPCommentReplyObject> createSOAPCommentReplyObjectElement(SOAPCommentReplyObject value) {
        return new JAXBElement<SOAPCommentReplyObject>(_SOAPCommentReplyObjectElement_QNAME, SOAPCommentReplyObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPWorkflowProofStageRequestObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPWorkflowProofStageRequestObjectArrayElement")
    public JAXBElement<SOAPWorkflowProofStageRequestObjectArray> createSOAPWorkflowProofStageRequestObjectArrayElement(SOAPWorkflowProofStageRequestObjectArray value) {
        return new JAXBElement<SOAPWorkflowProofStageRequestObjectArray>(_SOAPWorkflowProofStageRequestObjectArrayElement_QNAME, SOAPWorkflowProofStageRequestObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StringArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "StringArrayElement")
    public JAXBElement<StringArray> createStringArrayElement(StringArray value) {
        return new JAXBElement<StringArray>(_StringArrayElement_QNAME, StringArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPCallbackObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPCallbackObjectElement")
    public JAXBElement<SOAPCallbackObject> createSOAPCallbackObjectElement(SOAPCallbackObject value) {
        return new JAXBElement<SOAPCallbackObject>(_SOAPCallbackObjectElement_QNAME, SOAPCallbackObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPDecisionObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPDecisionObjectElement")
    public JAXBElement<SOAPDecisionObject> createSOAPDecisionObjectElement(SOAPDecisionObject value) {
        return new JAXBElement<SOAPDecisionObject>(_SOAPDecisionObjectElement_QNAME, SOAPDecisionObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPContactAccountObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPContactAccountObjectArrayElement")
    public JAXBElement<SOAPContactAccountObjectArray> createSOAPContactAccountObjectArrayElement(SOAPContactAccountObjectArray value) {
        return new JAXBElement<SOAPContactAccountObjectArray>(_SOAPContactAccountObjectArrayElement_QNAME, SOAPContactAccountObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPFileActivityObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPFileActivityObjectElement")
    public JAXBElement<SOAPFileActivityObject> createSOAPFileActivityObjectElement(SOAPFileActivityObject value) {
        return new JAXBElement<SOAPFileActivityObject>(_SOAPFileActivityObjectElement_QNAME, SOAPFileActivityObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPProfileObjectArray }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPProfileObjectArrayElement")
    public JAXBElement<SOAPProfileObjectArray> createSOAPProfileObjectArrayElement(SOAPProfileObjectArray value) {
        return new JAXBElement<SOAPProfileObjectArray>(_SOAPProfileObjectArrayElement_QNAME, SOAPProfileObjectArray.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPAccountObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPAccountObjectElement")
    public JAXBElement<SOAPAccountObject> createSOAPAccountObjectElement(SOAPAccountObject value) {
        return new JAXBElement<SOAPAccountObject>(_SOAPAccountObjectElement_QNAME, SOAPAccountObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOAPAccountProofingDefaultsObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://soap.proofhq.com/", name = "SOAPAccountProofingDefaultsObjectElement")
    public JAXBElement<SOAPAccountProofingDefaultsObject> createSOAPAccountProofingDefaultsObjectElement(SOAPAccountProofingDefaultsObject value) {
        return new JAXBElement<SOAPAccountProofingDefaultsObject>(_SOAPAccountProofingDefaultsObjectElement_QNAME, SOAPAccountProofingDefaultsObject.class, null, value);
    }

}
