
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowProofObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowProofObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proof_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filesize" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="versions" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="upload_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="uploader_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="uploader" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="owner_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stages" type="{https://soap.proofhq.com/}SOAPWorkflowProofStageObjectArray"/>
 *         &lt;element name="workspace_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="workspace_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="team_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="settings" type="{https://soap.proofhq.com/}SOAPFileSettingsObject"/>
 *         &lt;element name="custom_link_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="custom_link_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="menus_links" type="{https://soap.proofhq.com/}SOAPFileLinkObjectArray"/>
 *         &lt;element name="swf" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decision_reasons" type="{https://soap.proofhq.com/}SOAPRecipientDecisionReasonObjectArray"/>
 *         &lt;element name="combined_proof" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowProofObject", propOrder = {
    "proofId",
    "type",
    "filename",
    "filesize",
    "version",
    "versions",
    "uploadTime",
    "uploaderId",
    "uploader",
    "ownerId",
    "owner",
    "stages",
    "workspaceId",
    "workspaceName",
    "status",
    "decision",
    "teamUrl",
    "settings",
    "customLinkUrl",
    "customLinkLabel",
    "menusLinks",
    "swf",
    "decisionReasons",
    "combinedProof"
})
public class SOAPWorkflowProofObject {

    @XmlElement(name = "proof_id")
    protected int proofId;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String filename;
    @XmlElement(required = true)
    protected String filesize;
    protected int version;
    protected int versions;
    @XmlElement(name = "upload_time", required = true)
    protected String uploadTime;
    @XmlElement(name = "uploader_id")
    protected int uploaderId;
    @XmlElement(required = true)
    protected String uploader;
    @XmlElement(name = "owner_id")
    protected int ownerId;
    @XmlElement(required = true)
    protected String owner;
    @XmlElement(required = true)
    protected SOAPWorkflowProofStageObjectArray stages;
    @XmlElement(name = "workspace_id")
    protected int workspaceId;
    @XmlElement(name = "workspace_name", required = true)
    protected String workspaceName;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String decision;
    @XmlElement(name = "team_url", required = true)
    protected String teamUrl;
    @XmlElement(required = true)
    protected SOAPFileSettingsObject settings;
    @XmlElement(name = "custom_link_url", required = true)
    protected String customLinkUrl;
    @XmlElement(name = "custom_link_label", required = true)
    protected String customLinkLabel;
    @XmlElement(name = "menus_links", required = true)
    protected SOAPFileLinkObjectArray menusLinks;
    @XmlElement(required = true)
    protected String swf;
    @XmlElement(name = "decision_reasons", required = true)
    protected SOAPRecipientDecisionReasonObjectArray decisionReasons;
    @XmlElement(name = "combined_proof")
    protected boolean combinedProof;

    /**
     * Gets the value of the proofId property.
     * 
     */
    public int getProofId() {
        return proofId;
    }

    /**
     * Sets the value of the proofId property.
     * 
     */
    public void setProofId(int value) {
        this.proofId = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the filename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Sets the value of the filename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilename(String value) {
        this.filename = value;
    }

    /**
     * Gets the value of the filesize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilesize() {
        return filesize;
    }

    /**
     * Sets the value of the filesize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilesize(String value) {
        this.filesize = value;
    }

    /**
     * Gets the value of the version property.
     * 
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     */
    public void setVersion(int value) {
        this.version = value;
    }

    /**
     * Gets the value of the versions property.
     * 
     */
    public int getVersions() {
        return versions;
    }

    /**
     * Sets the value of the versions property.
     * 
     */
    public void setVersions(int value) {
        this.versions = value;
    }

    /**
     * Gets the value of the uploadTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUploadTime() {
        return uploadTime;
    }

    /**
     * Sets the value of the uploadTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUploadTime(String value) {
        this.uploadTime = value;
    }

    /**
     * Gets the value of the uploaderId property.
     * 
     */
    public int getUploaderId() {
        return uploaderId;
    }

    /**
     * Sets the value of the uploaderId property.
     * 
     */
    public void setUploaderId(int value) {
        this.uploaderId = value;
    }

    /**
     * Gets the value of the uploader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUploader() {
        return uploader;
    }

    /**
     * Sets the value of the uploader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUploader(String value) {
        this.uploader = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     */
    public int getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     */
    public void setOwnerId(int value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the stages property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPWorkflowProofStageObjectArray }
     *     
     */
    public SOAPWorkflowProofStageObjectArray getStages() {
        return stages;
    }

    /**
     * Sets the value of the stages property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPWorkflowProofStageObjectArray }
     *     
     */
    public void setStages(SOAPWorkflowProofStageObjectArray value) {
        this.stages = value;
    }

    /**
     * Gets the value of the workspaceId property.
     * 
     */
    public int getWorkspaceId() {
        return workspaceId;
    }

    /**
     * Sets the value of the workspaceId property.
     * 
     */
    public void setWorkspaceId(int value) {
        this.workspaceId = value;
    }

    /**
     * Gets the value of the workspaceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkspaceName() {
        return workspaceName;
    }

    /**
     * Sets the value of the workspaceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkspaceName(String value) {
        this.workspaceName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the teamUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTeamUrl() {
        return teamUrl;
    }

    /**
     * Sets the value of the teamUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTeamUrl(String value) {
        this.teamUrl = value;
    }

    /**
     * Gets the value of the settings property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPFileSettingsObject }
     *     
     */
    public SOAPFileSettingsObject getSettings() {
        return settings;
    }

    /**
     * Sets the value of the settings property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPFileSettingsObject }
     *     
     */
    public void setSettings(SOAPFileSettingsObject value) {
        this.settings = value;
    }

    /**
     * Gets the value of the customLinkUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomLinkUrl() {
        return customLinkUrl;
    }

    /**
     * Sets the value of the customLinkUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomLinkUrl(String value) {
        this.customLinkUrl = value;
    }

    /**
     * Gets the value of the customLinkLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomLinkLabel() {
        return customLinkLabel;
    }

    /**
     * Sets the value of the customLinkLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomLinkLabel(String value) {
        this.customLinkLabel = value;
    }

    /**
     * Gets the value of the menusLinks property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPFileLinkObjectArray }
     *     
     */
    public SOAPFileLinkObjectArray getMenusLinks() {
        return menusLinks;
    }

    /**
     * Sets the value of the menusLinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPFileLinkObjectArray }
     *     
     */
    public void setMenusLinks(SOAPFileLinkObjectArray value) {
        this.menusLinks = value;
    }

    /**
     * Gets the value of the swf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwf() {
        return swf;
    }

    /**
     * Sets the value of the swf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwf(String value) {
        this.swf = value;
    }

    /**
     * Gets the value of the decisionReasons property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPRecipientDecisionReasonObjectArray }
     *     
     */
    public SOAPRecipientDecisionReasonObjectArray getDecisionReasons() {
        return decisionReasons;
    }

    /**
     * Sets the value of the decisionReasons property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPRecipientDecisionReasonObjectArray }
     *     
     */
    public void setDecisionReasons(SOAPRecipientDecisionReasonObjectArray value) {
        this.decisionReasons = value;
    }

    /**
     * Gets the value of the combinedProof property.
     * 
     */
    public boolean isCombinedProof() {
        return combinedProof;
    }

    /**
     * Sets the value of the combinedProof property.
     * 
     */
    public void setCombinedProof(boolean value) {
        this.combinedProof = value;
    }

}
