
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPRecipientObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPRecipientObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="file_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="position" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="proof_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dowload_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadline" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="email_notifications" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="replies_count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="comments_count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="primary_decision_maker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decision_reasons" type="{https://soap.proofhq.com/}SOAPRecipientDecisionReasonObjectArray"/>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="progress" type="{https://soap.proofhq.com/}SOAPProgressObject"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPRecipientObject", propOrder = {
    "id",
    "fileId",
    "name",
    "email",
    "position",
    "role",
    "proofUrl",
    "dowloadUrl",
    "decision",
    "deadline",
    "emailNotifications",
    "repliesCount",
    "commentsCount",
    "primaryDecisionMaker",
    "decisionReasons",
    "stageId",
    "progress"
})
public class SOAPRecipientObject {

    protected int id;
    @XmlElement(name = "file_id")
    protected int fileId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(required = true)
    protected String position;
    @XmlElement(required = true)
    protected String role;
    @XmlElement(name = "proof_url", required = true)
    protected String proofUrl;
    @XmlElement(name = "dowload_url", required = true)
    protected String dowloadUrl;
    @XmlElement(required = true)
    protected String decision;
    @XmlElement(required = true)
    protected String deadline;
    @XmlElement(name = "email_notifications", required = true)
    protected String emailNotifications;
    @XmlElement(name = "replies_count")
    protected int repliesCount;
    @XmlElement(name = "comments_count")
    protected int commentsCount;
    @XmlElement(name = "primary_decision_maker", required = true)
    protected String primaryDecisionMaker;
    @XmlElement(name = "decision_reasons", required = true)
    protected SOAPRecipientDecisionReasonObjectArray decisionReasons;
    @XmlElement(name = "stage_id")
    protected int stageId;
    @XmlElement(required = true)
    protected SOAPProgressObject progress;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the fileId property.
     * 
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * Sets the value of the fileId property.
     * 
     */
    public void setFileId(int value) {
        this.fileId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the proofUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProofUrl() {
        return proofUrl;
    }

    /**
     * Sets the value of the proofUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProofUrl(String value) {
        this.proofUrl = value;
    }

    /**
     * Gets the value of the dowloadUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDowloadUrl() {
        return dowloadUrl;
    }

    /**
     * Sets the value of the dowloadUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDowloadUrl(String value) {
        this.dowloadUrl = value;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the deadline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     * Sets the value of the deadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadline(String value) {
        this.deadline = value;
    }

    /**
     * Gets the value of the emailNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailNotifications() {
        return emailNotifications;
    }

    /**
     * Sets the value of the emailNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailNotifications(String value) {
        this.emailNotifications = value;
    }

    /**
     * Gets the value of the repliesCount property.
     * 
     */
    public int getRepliesCount() {
        return repliesCount;
    }

    /**
     * Sets the value of the repliesCount property.
     * 
     */
    public void setRepliesCount(int value) {
        this.repliesCount = value;
    }

    /**
     * Gets the value of the commentsCount property.
     * 
     */
    public int getCommentsCount() {
        return commentsCount;
    }

    /**
     * Sets the value of the commentsCount property.
     * 
     */
    public void setCommentsCount(int value) {
        this.commentsCount = value;
    }

    /**
     * Gets the value of the primaryDecisionMaker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDecisionMaker() {
        return primaryDecisionMaker;
    }

    /**
     * Sets the value of the primaryDecisionMaker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDecisionMaker(String value) {
        this.primaryDecisionMaker = value;
    }

    /**
     * Gets the value of the decisionReasons property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPRecipientDecisionReasonObjectArray }
     *     
     */
    public SOAPRecipientDecisionReasonObjectArray getDecisionReasons() {
        return decisionReasons;
    }

    /**
     * Sets the value of the decisionReasons property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPRecipientDecisionReasonObjectArray }
     *     
     */
    public void setDecisionReasons(SOAPRecipientDecisionReasonObjectArray value) {
        this.decisionReasons = value;
    }

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the progress property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPProgressObject }
     *     
     */
    public SOAPProgressObject getProgress() {
        return progress;
    }

    /**
     * Sets the value of the progress property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPProgressObject }
     *     
     */
    public void setProgress(SOAPProgressObject value) {
        this.progress = value;
    }

}
