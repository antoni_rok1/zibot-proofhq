
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPAccountProofingDefaultsObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPAccountProofingDefaultsObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="on_load_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="on_decision_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="confirm_button" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cancel_button" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPAccountProofingDefaultsObject", propOrder = {
    "onLoadMessage",
    "onDecisionMessage",
    "confirmButton",
    "cancelButton"
})
public class SOAPAccountProofingDefaultsObject {

    @XmlElement(name = "on_load_message", required = true)
    protected String onLoadMessage;
    @XmlElement(name = "on_decision_message", required = true)
    protected String onDecisionMessage;
    @XmlElement(name = "confirm_button", required = true)
    protected String confirmButton;
    @XmlElement(name = "cancel_button", required = true)
    protected String cancelButton;

    /**
     * Gets the value of the onLoadMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnLoadMessage() {
        return onLoadMessage;
    }

    /**
     * Sets the value of the onLoadMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnLoadMessage(String value) {
        this.onLoadMessage = value;
    }

    /**
     * Gets the value of the onDecisionMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnDecisionMessage() {
        return onDecisionMessage;
    }

    /**
     * Sets the value of the onDecisionMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnDecisionMessage(String value) {
        this.onDecisionMessage = value;
    }

    /**
     * Gets the value of the confirmButton property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmButton() {
        return confirmButton;
    }

    /**
     * Sets the value of the confirmButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmButton(String value) {
        this.confirmButton = value;
    }

    /**
     * Gets the value of the cancelButton property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelButton() {
        return cancelButton;
    }

    /**
     * Sets the value of the cancelButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelButton(String value) {
        this.cancelButton = value;
    }

}
