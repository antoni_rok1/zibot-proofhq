
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPMembershipObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPMembershipObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="plan" type="{https://soap.proofhq.com/}SOAPPlanObject"/>
 *         &lt;element name="start_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="finish_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="storage_usage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="proofs_usage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="users_usage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="storage_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="proofs_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="users_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPMembershipObject", propOrder = {
    "id",
    "plan",
    "startAt",
    "finishAt",
    "storageUsage",
    "proofsUsage",
    "usersUsage",
    "storageLimit",
    "proofsLimit",
    "usersLimit"
})
public class SOAPMembershipObject {

    protected int id;
    @XmlElement(required = true)
    protected SOAPPlanObject plan;
    @XmlElement(name = "start_at", required = true)
    protected String startAt;
    @XmlElement(name = "finish_at", required = true)
    protected String finishAt;
    @XmlElement(name = "storage_usage")
    protected int storageUsage;
    @XmlElement(name = "proofs_usage")
    protected int proofsUsage;
    @XmlElement(name = "users_usage")
    protected int usersUsage;
    @XmlElement(name = "storage_limit")
    protected int storageLimit;
    @XmlElement(name = "proofs_limit")
    protected int proofsLimit;
    @XmlElement(name = "users_limit")
    protected int usersLimit;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the plan property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPPlanObject }
     *     
     */
    public SOAPPlanObject getPlan() {
        return plan;
    }

    /**
     * Sets the value of the plan property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPPlanObject }
     *     
     */
    public void setPlan(SOAPPlanObject value) {
        this.plan = value;
    }

    /**
     * Gets the value of the startAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartAt() {
        return startAt;
    }

    /**
     * Sets the value of the startAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartAt(String value) {
        this.startAt = value;
    }

    /**
     * Gets the value of the finishAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinishAt() {
        return finishAt;
    }

    /**
     * Sets the value of the finishAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinishAt(String value) {
        this.finishAt = value;
    }

    /**
     * Gets the value of the storageUsage property.
     * 
     */
    public int getStorageUsage() {
        return storageUsage;
    }

    /**
     * Sets the value of the storageUsage property.
     * 
     */
    public void setStorageUsage(int value) {
        this.storageUsage = value;
    }

    /**
     * Gets the value of the proofsUsage property.
     * 
     */
    public int getProofsUsage() {
        return proofsUsage;
    }

    /**
     * Sets the value of the proofsUsage property.
     * 
     */
    public void setProofsUsage(int value) {
        this.proofsUsage = value;
    }

    /**
     * Gets the value of the usersUsage property.
     * 
     */
    public int getUsersUsage() {
        return usersUsage;
    }

    /**
     * Sets the value of the usersUsage property.
     * 
     */
    public void setUsersUsage(int value) {
        this.usersUsage = value;
    }

    /**
     * Gets the value of the storageLimit property.
     * 
     */
    public int getStorageLimit() {
        return storageLimit;
    }

    /**
     * Sets the value of the storageLimit property.
     * 
     */
    public void setStorageLimit(int value) {
        this.storageLimit = value;
    }

    /**
     * Gets the value of the proofsLimit property.
     * 
     */
    public int getProofsLimit() {
        return proofsLimit;
    }

    /**
     * Sets the value of the proofsLimit property.
     * 
     */
    public void setProofsLimit(int value) {
        this.proofsLimit = value;
    }

    /**
     * Gets the value of the usersLimit property.
     * 
     */
    public int getUsersLimit() {
        return usersLimit;
    }

    /**
     * Sets the value of the usersLimit property.
     * 
     */
    public void setUsersLimit(int value) {
        this.usersLimit = value;
    }

}
