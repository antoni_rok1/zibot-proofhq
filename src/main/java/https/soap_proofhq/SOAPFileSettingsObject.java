
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPFileSettingsObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPFileSettingsObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="default_email_notifications" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="default_role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authorized_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enable_subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="validate_subscriptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="auto_lock" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lock_after_first_decision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enable_download" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enable_team_url" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enable_embed_player" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_dashboard_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_workspace_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_details_page_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_versions_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_quick_start_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_user_guide_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_welcome_screen_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_forum_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_help_blog_link" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_dashboard_functions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="primary_decision_maker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="show_close_icon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_publish_to_the_web" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_print_comments" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_email_alert_settings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="electronic_signatures" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_send_decision_confirmation_checkbox" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_proof_details" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="show_proof_workflow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPFileSettingsObject", propOrder = {
    "defaultEmailNotifications",
    "defaultRole",
    "authorizedOnly",
    "enableSubscriptions",
    "validateSubscriptions",
    "autoLock",
    "lockAfterFirstDecision",
    "enableDownload",
    "enableTeamUrl",
    "enableEmbedPlayer",
    "showDashboardLink",
    "showWorkspaceLink",
    "showDetailsPageLink",
    "showVersionsLink",
    "showHelpLink",
    "showHelpQuickStartLink",
    "showHelpUserGuideLink",
    "showHelpWelcomeScreenLink",
    "showHelpForumLink",
    "showHelpBlogLink",
    "showDashboardFunctions",
    "primaryDecisionMaker",
    "showCloseIcon",
    "showPublishToTheWeb",
    "showPrintComments",
    "showEmailAlertSettings",
    "electronicSignatures",
    "showSendDecisionConfirmationCheckbox",
    "showProofDetails",
    "showProofWorkflow"
})
public class SOAPFileSettingsObject {

    @XmlElement(name = "default_email_notifications", required = true)
    protected String defaultEmailNotifications;
    @XmlElement(name = "default_role", required = true)
    protected String defaultRole;
    @XmlElement(name = "authorized_only")
    protected boolean authorizedOnly;
    @XmlElement(name = "enable_subscriptions")
    protected boolean enableSubscriptions;
    @XmlElement(name = "validate_subscriptions")
    protected boolean validateSubscriptions;
    @XmlElement(name = "auto_lock")
    protected boolean autoLock;
    @XmlElement(name = "lock_after_first_decision")
    protected boolean lockAfterFirstDecision;
    @XmlElement(name = "enable_download")
    protected boolean enableDownload;
    @XmlElement(name = "enable_team_url")
    protected boolean enableTeamUrl;
    @XmlElement(name = "enable_embed_player")
    protected boolean enableEmbedPlayer;
    @XmlElement(name = "show_dashboard_link")
    protected boolean showDashboardLink;
    @XmlElement(name = "show_workspace_link")
    protected boolean showWorkspaceLink;
    @XmlElement(name = "show_details_page_link")
    protected boolean showDetailsPageLink;
    @XmlElement(name = "show_versions_link")
    protected boolean showVersionsLink;
    @XmlElement(name = "show_help_link")
    protected boolean showHelpLink;
    @XmlElement(name = "show_help_quick_start_link")
    protected boolean showHelpQuickStartLink;
    @XmlElement(name = "show_help_user_guide_link")
    protected boolean showHelpUserGuideLink;
    @XmlElement(name = "show_help_welcome_screen_link")
    protected boolean showHelpWelcomeScreenLink;
    @XmlElement(name = "show_help_forum_link")
    protected boolean showHelpForumLink;
    @XmlElement(name = "show_help_blog_link")
    protected boolean showHelpBlogLink;
    @XmlElement(name = "show_dashboard_functions")
    protected boolean showDashboardFunctions;
    @XmlElement(name = "primary_decision_maker", required = true)
    protected String primaryDecisionMaker;
    @XmlElement(name = "show_close_icon")
    protected boolean showCloseIcon;
    @XmlElement(name = "show_publish_to_the_web")
    protected boolean showPublishToTheWeb;
    @XmlElement(name = "show_print_comments")
    protected boolean showPrintComments;
    @XmlElement(name = "show_email_alert_settings")
    protected boolean showEmailAlertSettings;
    @XmlElement(name = "electronic_signatures")
    protected boolean electronicSignatures;
    @XmlElement(name = "show_send_decision_confirmation_checkbox")
    protected boolean showSendDecisionConfirmationCheckbox;
    @XmlElement(name = "show_proof_details")
    protected boolean showProofDetails;
    @XmlElement(name = "show_proof_workflow")
    protected boolean showProofWorkflow;

    /**
     * Gets the value of the defaultEmailNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultEmailNotifications() {
        return defaultEmailNotifications;
    }

    /**
     * Sets the value of the defaultEmailNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultEmailNotifications(String value) {
        this.defaultEmailNotifications = value;
    }

    /**
     * Gets the value of the defaultRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRole() {
        return defaultRole;
    }

    /**
     * Sets the value of the defaultRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRole(String value) {
        this.defaultRole = value;
    }

    /**
     * Gets the value of the authorizedOnly property.
     * 
     */
    public boolean isAuthorizedOnly() {
        return authorizedOnly;
    }

    /**
     * Sets the value of the authorizedOnly property.
     * 
     */
    public void setAuthorizedOnly(boolean value) {
        this.authorizedOnly = value;
    }

    /**
     * Gets the value of the enableSubscriptions property.
     * 
     */
    public boolean isEnableSubscriptions() {
        return enableSubscriptions;
    }

    /**
     * Sets the value of the enableSubscriptions property.
     * 
     */
    public void setEnableSubscriptions(boolean value) {
        this.enableSubscriptions = value;
    }

    /**
     * Gets the value of the validateSubscriptions property.
     * 
     */
    public boolean isValidateSubscriptions() {
        return validateSubscriptions;
    }

    /**
     * Sets the value of the validateSubscriptions property.
     * 
     */
    public void setValidateSubscriptions(boolean value) {
        this.validateSubscriptions = value;
    }

    /**
     * Gets the value of the autoLock property.
     * 
     */
    public boolean isAutoLock() {
        return autoLock;
    }

    /**
     * Sets the value of the autoLock property.
     * 
     */
    public void setAutoLock(boolean value) {
        this.autoLock = value;
    }

    /**
     * Gets the value of the lockAfterFirstDecision property.
     * 
     */
    public boolean isLockAfterFirstDecision() {
        return lockAfterFirstDecision;
    }

    /**
     * Sets the value of the lockAfterFirstDecision property.
     * 
     */
    public void setLockAfterFirstDecision(boolean value) {
        this.lockAfterFirstDecision = value;
    }

    /**
     * Gets the value of the enableDownload property.
     * 
     */
    public boolean isEnableDownload() {
        return enableDownload;
    }

    /**
     * Sets the value of the enableDownload property.
     * 
     */
    public void setEnableDownload(boolean value) {
        this.enableDownload = value;
    }

    /**
     * Gets the value of the enableTeamUrl property.
     * 
     */
    public boolean isEnableTeamUrl() {
        return enableTeamUrl;
    }

    /**
     * Sets the value of the enableTeamUrl property.
     * 
     */
    public void setEnableTeamUrl(boolean value) {
        this.enableTeamUrl = value;
    }

    /**
     * Gets the value of the enableEmbedPlayer property.
     * 
     */
    public boolean isEnableEmbedPlayer() {
        return enableEmbedPlayer;
    }

    /**
     * Sets the value of the enableEmbedPlayer property.
     * 
     */
    public void setEnableEmbedPlayer(boolean value) {
        this.enableEmbedPlayer = value;
    }

    /**
     * Gets the value of the showDashboardLink property.
     * 
     */
    public boolean isShowDashboardLink() {
        return showDashboardLink;
    }

    /**
     * Sets the value of the showDashboardLink property.
     * 
     */
    public void setShowDashboardLink(boolean value) {
        this.showDashboardLink = value;
    }

    /**
     * Gets the value of the showWorkspaceLink property.
     * 
     */
    public boolean isShowWorkspaceLink() {
        return showWorkspaceLink;
    }

    /**
     * Sets the value of the showWorkspaceLink property.
     * 
     */
    public void setShowWorkspaceLink(boolean value) {
        this.showWorkspaceLink = value;
    }

    /**
     * Gets the value of the showDetailsPageLink property.
     * 
     */
    public boolean isShowDetailsPageLink() {
        return showDetailsPageLink;
    }

    /**
     * Sets the value of the showDetailsPageLink property.
     * 
     */
    public void setShowDetailsPageLink(boolean value) {
        this.showDetailsPageLink = value;
    }

    /**
     * Gets the value of the showVersionsLink property.
     * 
     */
    public boolean isShowVersionsLink() {
        return showVersionsLink;
    }

    /**
     * Sets the value of the showVersionsLink property.
     * 
     */
    public void setShowVersionsLink(boolean value) {
        this.showVersionsLink = value;
    }

    /**
     * Gets the value of the showHelpLink property.
     * 
     */
    public boolean isShowHelpLink() {
        return showHelpLink;
    }

    /**
     * Sets the value of the showHelpLink property.
     * 
     */
    public void setShowHelpLink(boolean value) {
        this.showHelpLink = value;
    }

    /**
     * Gets the value of the showHelpQuickStartLink property.
     * 
     */
    public boolean isShowHelpQuickStartLink() {
        return showHelpQuickStartLink;
    }

    /**
     * Sets the value of the showHelpQuickStartLink property.
     * 
     */
    public void setShowHelpQuickStartLink(boolean value) {
        this.showHelpQuickStartLink = value;
    }

    /**
     * Gets the value of the showHelpUserGuideLink property.
     * 
     */
    public boolean isShowHelpUserGuideLink() {
        return showHelpUserGuideLink;
    }

    /**
     * Sets the value of the showHelpUserGuideLink property.
     * 
     */
    public void setShowHelpUserGuideLink(boolean value) {
        this.showHelpUserGuideLink = value;
    }

    /**
     * Gets the value of the showHelpWelcomeScreenLink property.
     * 
     */
    public boolean isShowHelpWelcomeScreenLink() {
        return showHelpWelcomeScreenLink;
    }

    /**
     * Sets the value of the showHelpWelcomeScreenLink property.
     * 
     */
    public void setShowHelpWelcomeScreenLink(boolean value) {
        this.showHelpWelcomeScreenLink = value;
    }

    /**
     * Gets the value of the showHelpForumLink property.
     * 
     */
    public boolean isShowHelpForumLink() {
        return showHelpForumLink;
    }

    /**
     * Sets the value of the showHelpForumLink property.
     * 
     */
    public void setShowHelpForumLink(boolean value) {
        this.showHelpForumLink = value;
    }

    /**
     * Gets the value of the showHelpBlogLink property.
     * 
     */
    public boolean isShowHelpBlogLink() {
        return showHelpBlogLink;
    }

    /**
     * Sets the value of the showHelpBlogLink property.
     * 
     */
    public void setShowHelpBlogLink(boolean value) {
        this.showHelpBlogLink = value;
    }

    /**
     * Gets the value of the showDashboardFunctions property.
     * 
     */
    public boolean isShowDashboardFunctions() {
        return showDashboardFunctions;
    }

    /**
     * Sets the value of the showDashboardFunctions property.
     * 
     */
    public void setShowDashboardFunctions(boolean value) {
        this.showDashboardFunctions = value;
    }

    /**
     * Gets the value of the primaryDecisionMaker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDecisionMaker() {
        return primaryDecisionMaker;
    }

    /**
     * Sets the value of the primaryDecisionMaker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDecisionMaker(String value) {
        this.primaryDecisionMaker = value;
    }

    /**
     * Gets the value of the showCloseIcon property.
     * 
     */
    public boolean isShowCloseIcon() {
        return showCloseIcon;
    }

    /**
     * Sets the value of the showCloseIcon property.
     * 
     */
    public void setShowCloseIcon(boolean value) {
        this.showCloseIcon = value;
    }

    /**
     * Gets the value of the showPublishToTheWeb property.
     * 
     */
    public boolean isShowPublishToTheWeb() {
        return showPublishToTheWeb;
    }

    /**
     * Sets the value of the showPublishToTheWeb property.
     * 
     */
    public void setShowPublishToTheWeb(boolean value) {
        this.showPublishToTheWeb = value;
    }

    /**
     * Gets the value of the showPrintComments property.
     * 
     */
    public boolean isShowPrintComments() {
        return showPrintComments;
    }

    /**
     * Sets the value of the showPrintComments property.
     * 
     */
    public void setShowPrintComments(boolean value) {
        this.showPrintComments = value;
    }

    /**
     * Gets the value of the showEmailAlertSettings property.
     * 
     */
    public boolean isShowEmailAlertSettings() {
        return showEmailAlertSettings;
    }

    /**
     * Sets the value of the showEmailAlertSettings property.
     * 
     */
    public void setShowEmailAlertSettings(boolean value) {
        this.showEmailAlertSettings = value;
    }

    /**
     * Gets the value of the electronicSignatures property.
     * 
     */
    public boolean isElectronicSignatures() {
        return electronicSignatures;
    }

    /**
     * Sets the value of the electronicSignatures property.
     * 
     */
    public void setElectronicSignatures(boolean value) {
        this.electronicSignatures = value;
    }

    /**
     * Gets the value of the showSendDecisionConfirmationCheckbox property.
     * 
     */
    public boolean isShowSendDecisionConfirmationCheckbox() {
        return showSendDecisionConfirmationCheckbox;
    }

    /**
     * Sets the value of the showSendDecisionConfirmationCheckbox property.
     * 
     */
    public void setShowSendDecisionConfirmationCheckbox(boolean value) {
        this.showSendDecisionConfirmationCheckbox = value;
    }

    /**
     * Gets the value of the showProofDetails property.
     * 
     */
    public boolean isShowProofDetails() {
        return showProofDetails;
    }

    /**
     * Sets the value of the showProofDetails property.
     * 
     */
    public void setShowProofDetails(boolean value) {
        this.showProofDetails = value;
    }

    /**
     * Gets the value of the showProofWorkflow property.
     * 
     */
    public boolean isShowProofWorkflow() {
        return showProofWorkflow;
    }

    /**
     * Sets the value of the showProofWorkflow property.
     * 
     */
    public void setShowProofWorkflow(boolean value) {
        this.showProofWorkflow = value;
    }

}
