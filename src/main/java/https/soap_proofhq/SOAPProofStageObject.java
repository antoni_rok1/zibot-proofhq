
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPProofStageObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPProofStageObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="file_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="progress" type="{https://soap.proofhq.com/}SOAPProgressObject"/>
 *         &lt;element name="deadline_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="start_trigger" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_stage_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="start_dependent_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stage_locking" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stage_primary_decision_maker_recipient_id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="stage_one_decision_only" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stage_private" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="comments_count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="recipients_count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stage_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="last_status_change_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadline_calculate_on" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPProofStageObject", propOrder = {
    "fileId",
    "stageId",
    "name",
    "progress",
    "deadlineDate",
    "startTrigger",
    "startDependentStageId",
    "startDependentDate",
    "stageLocking",
    "stagePrimaryDecisionMakerRecipientId",
    "stageOneDecisionOnly",
    "stagePrivate",
    "commentsCount",
    "recipientsCount",
    "decision",
    "stageStartDate",
    "lastStatusChangeDate",
    "deadlineCalculateOn"
})
public class SOAPProofStageObject {

    @XmlElement(name = "file_id")
    protected int fileId;
    @XmlElement(name = "stage_id")
    protected int stageId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected SOAPProgressObject progress;
    @XmlElement(name = "deadline_date", required = true)
    protected String deadlineDate;
    @XmlElement(name = "start_trigger")
    protected int startTrigger;
    @XmlElement(name = "start_dependent_stage_id")
    protected int startDependentStageId;
    @XmlElement(name = "start_dependent_date", required = true)
    protected String startDependentDate;
    @XmlElement(name = "stage_locking")
    protected int stageLocking;
    @XmlElement(name = "stage_primary_decision_maker_recipient_id")
    protected Integer stagePrimaryDecisionMakerRecipientId;
    @XmlElement(name = "stage_one_decision_only")
    protected boolean stageOneDecisionOnly;
    @XmlElement(name = "stage_private")
    protected boolean stagePrivate;
    @XmlElement(name = "comments_count")
    protected int commentsCount;
    @XmlElement(name = "recipients_count")
    protected int recipientsCount;
    @XmlElement(required = true)
    protected String decision;
    @XmlElement(name = "stage_start_date", required = true)
    protected String stageStartDate;
    @XmlElement(name = "last_status_change_date", required = true)
    protected String lastStatusChangeDate;
    @XmlElement(name = "deadline_calculate_on")
    protected Integer deadlineCalculateOn;

    /**
     * Gets the value of the fileId property.
     * 
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * Sets the value of the fileId property.
     * 
     */
    public void setFileId(int value) {
        this.fileId = value;
    }

    /**
     * Gets the value of the stageId property.
     * 
     */
    public int getStageId() {
        return stageId;
    }

    /**
     * Sets the value of the stageId property.
     * 
     */
    public void setStageId(int value) {
        this.stageId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the progress property.
     * 
     * @return
     *     possible object is
     *     {@link SOAPProgressObject }
     *     
     */
    public SOAPProgressObject getProgress() {
        return progress;
    }

    /**
     * Sets the value of the progress property.
     * 
     * @param value
     *     allowed object is
     *     {@link SOAPProgressObject }
     *     
     */
    public void setProgress(SOAPProgressObject value) {
        this.progress = value;
    }

    /**
     * Gets the value of the deadlineDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadlineDate() {
        return deadlineDate;
    }

    /**
     * Sets the value of the deadlineDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadlineDate(String value) {
        this.deadlineDate = value;
    }

    /**
     * Gets the value of the startTrigger property.
     * 
     */
    public int getStartTrigger() {
        return startTrigger;
    }

    /**
     * Sets the value of the startTrigger property.
     * 
     */
    public void setStartTrigger(int value) {
        this.startTrigger = value;
    }

    /**
     * Gets the value of the startDependentStageId property.
     * 
     */
    public int getStartDependentStageId() {
        return startDependentStageId;
    }

    /**
     * Sets the value of the startDependentStageId property.
     * 
     */
    public void setStartDependentStageId(int value) {
        this.startDependentStageId = value;
    }

    /**
     * Gets the value of the startDependentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDependentDate() {
        return startDependentDate;
    }

    /**
     * Sets the value of the startDependentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDependentDate(String value) {
        this.startDependentDate = value;
    }

    /**
     * Gets the value of the stageLocking property.
     * 
     */
    public int getStageLocking() {
        return stageLocking;
    }

    /**
     * Sets the value of the stageLocking property.
     * 
     */
    public void setStageLocking(int value) {
        this.stageLocking = value;
    }

    /**
     * Gets the value of the stagePrimaryDecisionMakerRecipientId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStagePrimaryDecisionMakerRecipientId() {
        return stagePrimaryDecisionMakerRecipientId;
    }

    /**
     * Sets the value of the stagePrimaryDecisionMakerRecipientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStagePrimaryDecisionMakerRecipientId(Integer value) {
        this.stagePrimaryDecisionMakerRecipientId = value;
    }

    /**
     * Gets the value of the stageOneDecisionOnly property.
     * 
     */
    public boolean isStageOneDecisionOnly() {
        return stageOneDecisionOnly;
    }

    /**
     * Sets the value of the stageOneDecisionOnly property.
     * 
     */
    public void setStageOneDecisionOnly(boolean value) {
        this.stageOneDecisionOnly = value;
    }

    /**
     * Gets the value of the stagePrivate property.
     * 
     */
    public boolean isStagePrivate() {
        return stagePrivate;
    }

    /**
     * Sets the value of the stagePrivate property.
     * 
     */
    public void setStagePrivate(boolean value) {
        this.stagePrivate = value;
    }

    /**
     * Gets the value of the commentsCount property.
     * 
     */
    public int getCommentsCount() {
        return commentsCount;
    }

    /**
     * Sets the value of the commentsCount property.
     * 
     */
    public void setCommentsCount(int value) {
        this.commentsCount = value;
    }

    /**
     * Gets the value of the recipientsCount property.
     * 
     */
    public int getRecipientsCount() {
        return recipientsCount;
    }

    /**
     * Sets the value of the recipientsCount property.
     * 
     */
    public void setRecipientsCount(int value) {
        this.recipientsCount = value;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the stageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStageStartDate() {
        return stageStartDate;
    }

    /**
     * Sets the value of the stageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStageStartDate(String value) {
        this.stageStartDate = value;
    }

    /**
     * Gets the value of the lastStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastStatusChangeDate() {
        return lastStatusChangeDate;
    }

    /**
     * Sets the value of the lastStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastStatusChangeDate(String value) {
        this.lastStatusChangeDate = value;
    }

    /**
     * Gets the value of the deadlineCalculateOn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeadlineCalculateOn() {
        return deadlineCalculateOn;
    }

    /**
     * Sets the value of the deadlineCalculateOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeadlineCalculateOn(Integer value) {
        this.deadlineCalculateOn = value;
    }

}
