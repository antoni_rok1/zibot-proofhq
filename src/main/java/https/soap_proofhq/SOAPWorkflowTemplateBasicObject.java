
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPWorkflowTemplateBasicObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPWorkflowTemplateBasicObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workflow_template_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="owner_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shared_users" type="{https://soap.proofhq.com/}IntArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPWorkflowTemplateBasicObject", propOrder = {
    "workflowTemplateId",
    "ownerId",
    "name",
    "sharedUsers"
})
public class SOAPWorkflowTemplateBasicObject {

    @XmlElement(name = "workflow_template_id")
    protected int workflowTemplateId;
    @XmlElement(name = "owner_id")
    protected int ownerId;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(name = "shared_users", required = true)
    protected IntArray sharedUsers;

    /**
     * Gets the value of the workflowTemplateId property.
     * 
     */
    public int getWorkflowTemplateId() {
        return workflowTemplateId;
    }

    /**
     * Sets the value of the workflowTemplateId property.
     * 
     */
    public void setWorkflowTemplateId(int value) {
        this.workflowTemplateId = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     */
    public int getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     */
    public void setOwnerId(int value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the sharedUsers property.
     * 
     * @return
     *     possible object is
     *     {@link IntArray }
     *     
     */
    public IntArray getSharedUsers() {
        return sharedUsers;
    }

    /**
     * Sets the value of the sharedUsers property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntArray }
     *     
     */
    public void setSharedUsers(IntArray value) {
        this.sharedUsers = value;
    }

}
