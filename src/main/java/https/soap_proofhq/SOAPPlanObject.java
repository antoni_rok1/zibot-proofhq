
package https.soap_proofhq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOAPPlanObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOAPPlanObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="enterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="user_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="storage_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="proof_limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOAPPlanObject", propOrder = {
    "id",
    "name",
    "price",
    "enterprise",
    "userLimit",
    "storageLimit",
    "proofLimit",
    "type"
})
public class SOAPPlanObject {

    protected int id;
    @XmlElement(required = true)
    protected String name;
    protected double price;
    protected boolean enterprise;
    @XmlElement(name = "user_limit")
    protected int userLimit;
    @XmlElement(name = "storage_limit")
    protected int storageLimit;
    @XmlElement(name = "proof_limit")
    protected int proofLimit;
    protected int type;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the price property.
     * 
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     */
    public void setPrice(double value) {
        this.price = value;
    }

    /**
     * Gets the value of the enterprise property.
     * 
     */
    public boolean isEnterprise() {
        return enterprise;
    }

    /**
     * Sets the value of the enterprise property.
     * 
     */
    public void setEnterprise(boolean value) {
        this.enterprise = value;
    }

    /**
     * Gets the value of the userLimit property.
     * 
     */
    public int getUserLimit() {
        return userLimit;
    }

    /**
     * Sets the value of the userLimit property.
     * 
     */
    public void setUserLimit(int value) {
        this.userLimit = value;
    }

    /**
     * Gets the value of the storageLimit property.
     * 
     */
    public int getStorageLimit() {
        return storageLimit;
    }

    /**
     * Sets the value of the storageLimit property.
     * 
     */
    public void setStorageLimit(int value) {
        this.storageLimit = value;
    }

    /**
     * Gets the value of the proofLimit property.
     * 
     */
    public int getProofLimit() {
        return proofLimit;
    }

    /**
     * Sets the value of the proofLimit property.
     * 
     */
    public void setProofLimit(int value) {
        this.proofLimit = value;
    }

    /**
     * Gets the value of the type property.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }

}
