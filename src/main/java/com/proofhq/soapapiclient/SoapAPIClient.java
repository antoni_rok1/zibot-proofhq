package com.proofhq.soapapiclient;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import https.soap_proofhq.SOAPFileObject;
import https.soap_proofhq.SOAPLoginObject;
import https.soap_proofhq.SoapPortType;
import https.soap_proofhq.SoapService;

public class SoapAPIClient {

	private static final String DONE_N = "Done%n";
	private static final String RESPONSE_TAG_FILENAME = "filename";
	private static final String RESPONSE_TAG_FILEHASH = "filehash";

	static final String UPLOAD_URL = "http://soap.proofhq.com/upload.php";

	private SoapService service;
	private SoapPortType port;

	public SoapAPIClient() {
		service = new SoapService();
		port = service.getSoapPort();
	}

	public SOAPLoginObject login(String login, String password) {
		return port.doLogin(login, password);
	}

	public boolean logout(String sessionID) {
		return port.doLogout(sessionID);
	}

	public int startBatch(String sessionID) {
		return port.createBatch(sessionID);
	}

	public boolean finishBatch(String sessionID, int batchID) {
		return port.finishBatch(sessionID, batchID);
	}

	public Map<String, String> upload(File file)
			throws IOException, SOAPException, ParserConfigurationException, SAXException {
		Map<String, String> files = new HashMap<>();

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(UPLOAD_URL);

		MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
		entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		entityBuilder.addPart(file.getName(), new FileBody(file));
		post.setEntity(entityBuilder.build());

		HttpResponse response = client.execute(post);
		int status = response.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			onUploaded(files, response);
		} else {
			// should probably throw something here
			System.err.println("Status code: " + status);
		}
		return files;
	}

	private void onUploaded(Map<String, String> files, HttpResponse response)
			throws ParserConfigurationException, SAXException, IOException {
		// Need to parse the XML response here
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		Document doc = docBuilder.parse(response.getEntity().getContent());
		NodeList nodes = doc.getElementsByTagName("item");
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			NodeList itemNodes = n.getChildNodes();
			String name = null;
			String hash = null;
			for (int j = 0; j < itemNodes.getLength(); j++) {
				Element tag = (Element) itemNodes.item(j);
				if (RESPONSE_TAG_FILENAME.equals(tag.getTagName())) {
					name = tag.getFirstChild().getTextContent();
				} else if (RESPONSE_TAG_FILEHASH.equals(tag.getTagName())) {
					hash = tag.getFirstChild().getTextContent();
				}
			}
			if (name != null && hash != null) {
				files.put(name, hash);
			}
		}
	};

	public void createProof(SOAPLoginObject login, Map<String, String> hashes, int batchId) {
		for (String filename : hashes.keySet()) {
			boolean enableSubscriptions = false;
			boolean enableSubscriptionsValidation = false;
			boolean authorizedOnly = false;
			boolean enableAutoLock = false;
			boolean lockAfterFirstDecision = false;
			boolean enableDownload = false;
			boolean enableTeamURL = false;
			boolean enableEmbedPlayer = false;
			boolean showDashboardLink = false;
			boolean showWorkspaceLink = false;
			boolean showDetailsPageLink = false;
			boolean showVersionsLink = false;
			boolean showHelpLink = false;
			boolean showHelpQuickStartLink = false;
			boolean showHelpUserGuideLink = false;
			boolean showHelpWelcomeScreenLink = false;
			boolean showHelpForumLink = false;
			boolean showHelpBlogLink = false;
			boolean suppressNewProofNotification = false;
			boolean showDashboardFunctions = false;
			boolean showCloseIcon = false;
			boolean showPublishToTheWeb = false;
			boolean showPrintComments = false;
			boolean showEmailAlertSettings = false;
			boolean enableElectronicSignatures = false;
			boolean showSendDecisionConfirmationCheckbox = false;
			boolean classicProofViewer = false;
			boolean disableSubscriptionEmail = false;
			boolean disableExcelSummary = false;
			boolean disablePdfSummary = false;
			boolean showProofDetails = false;
			boolean showProofWorkflow = false;
			SOAPFileObject createProof = port.createProof(login.getSession(), login.getUserId(), hashes.get(filename),
					filename, "sourcename", login.getDefaultSubject(), login.getDefaultMessage(), "", "", 0, batchId,
					enableSubscriptions, enableSubscriptionsValidation, login.getDefaultEmailNotifications(), "5",
					authorizedOnly, enableAutoLock, lockAfterFirstDecision, enableDownload, enableTeamURL,
					enableEmbedPlayer, showDashboardLink, showWorkspaceLink, showDetailsPageLink, showVersionsLink,
					showHelpLink, showHelpQuickStartLink, showHelpUserGuideLink, showHelpWelcomeScreenLink,
					showHelpForumLink, showHelpBlogLink, "", "", suppressNewProofNotification, "",
					showDashboardFunctions, showCloseIcon, showPublishToTheWeb, showPrintComments,
					showEmailAlertSettings, enableElectronicSignatures, showSendDecisionConfirmationCheckbox,
					classicProofViewer, disableSubscriptionEmail, disableExcelSummary, disablePdfSummary,
					showProofDetails, showProofWorkflow);

			System.out.println(
					"createProof = " + ToStringBuilder.reflectionToString(createProof, ToStringStyle.MULTI_LINE_STYLE));

		}
	}

	public static void main(String[] args)
			throws IOException, SOAPException, ParserConfigurationException, SAXException {

		printf("Username: ");
		String username = "antoni.rokitnicki@ziflow.com";
		printf("Password: ");
		String password = "MzKDMkP1ScnJ";

		SoapAPIClient client = new SoapAPIClient();
		printf("Logging in... ");
		SOAPLoginObject login = client.login(username, password);
		printf(DONE_N);

		printf("Starting batch... ");
		int batchId = client.startBatch(login.getSession());
		printf("Id: %d%n", batchId);

		printf("File to upload: ");
		String filename = "D:\\___ Docs\\hazelcast\\Hazelcast_Deployment_And_Operations_Guide_v1.1_spot.pdf";

		printf("Uploading file... ");
		Map<String, String> hashes = client.upload(new File(filename));
		printf(DONE_N + hashes);

		printf("Creating proof... ");
		client.createProof(login, hashes, batchId);
		printf(DONE_N);

		printf("Finishing batch... ");
		boolean finished = client.finishBatch(login.getSession(), batchId);
		printf(DONE_N + finished);

		printf("Logging out... ");
		client.logout(login.getSession());
		printf(DONE_N);
	}

	public static void printf(String format, Object... args) {
		System.out.printf(format, args);
	}

}
